﻿using Btcc.Infraestrutura.AOP;
using Btcc.Infraestrutura.Atributos;
using Btcc.Infraestrutura.PageObjects;
using OpenQA.Selenium;

namespace Btcc.EuResolvo.Dominio.PageObjects
{
    public class FerramentaLoginPage : LoginPage<FerramentaLoginPage, FerramentaConsultaDetalhesProtocoloPage>
    {
        protected override By ByBotaoLogin { get { return Constantes.ByComponentes.BotaoLogin; } }

        protected override By ByBotaoLogout { get { return Constantes.ByComponentes.BotaoLogout; } }

        protected override By ByLogin { get { return Constantes.ByComponentes.CampoLogin; } }

        protected override By BySenha { get { return Constantes.ByComponentes.CampoSenha; } }

        protected override string LoginUrl { get { return Constantes.Links.Login; } }

        public FerramentaLoginPage(IWebDriver driver) : base(driver)
        {
        }

        [UrlRequerida(Constantes.Links.Login)]
        public override FerramentaConsultaDetalhesProtocoloPage EfetueLogin(string login, string senha)
        {
            EscrevaEm(ByLogin, login);
            EscrevaEm(BySenha, senha);
            CliqueViaJs(ByBotaoLogin);
            return FabricaAOP.GereInstancia<FerramentaConsultaDetalhesProtocoloPage>(Driver, new ChecagemDeTelaWeb(Driver));
        }

        [AguardeCarregamento]
        public virtual void RealizeLogout()
        {
            CliqueViaJs(Constantes.ByComponentes.BotaoLogout);
        }

        public override bool UsuarioEstaLogado()
        {
            return Driver.Url == Constantes.Links.FornecedorConsultar;
        }
    }
}
