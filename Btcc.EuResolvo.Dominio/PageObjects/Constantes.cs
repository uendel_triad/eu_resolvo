﻿using OpenQA.Selenium;

namespace Btcc.EuResolvo.Dominio.PageObjects
{
    public static class Constantes
    {
        public static class Links
        {
            public const string Login = "https://www.consumidor.gov.br/pages/administrativo/login";
            public const string FornecedorConsultar = "https://www.consumidor.gov.br/pages/reclamacao/fornecedor/consultar/";
            public const string PrincipalRepresentante = "https://www.consumidor.gov.br/pages/principal/representante";
        }

        public static class ByComponentes
        {
            public static By CampoLogin = By.Name("j_username");
            public static By CampoSenha = By.Name("j_password");
            public static By BotaoLogin = By.Id("btnLoginPageForm");
            public static By BotaoLogout = By.ClassName("pull-right");

            public static By ComboFornecedorGerenciar = By.Id("menu_sel_fornecedor");
            public static By ComboConsultaQuantidade = By.Name("tbconsulta_length");

            public static By TogglePesquisar = By.ClassName("btn btn-sm btn-default dropdown-toggle");
            public static By BotaoLimpar = By.Id("btn-limpar");
            public static By BotaoPesquisar = By.Id("btn-pesquisar");
            public static By CampoProtocolo = By.Name("numeroProtocoloFormatado");
            public static By CampoDataInicioPeriodo = By.Id("dataIniPeriodo");
            public static By CampoHoraInicioPeriodo = By.Id("horaIniPeriodo");
            public static By CampoDataFimPeriodo = By.Id("dataFimPeriodo");
            public static By CampoHoraFimPeriodo = By.Id("horaFimPeriodo");

            public static By InformacoesDoProtocolo = By.ClassName("fundo-branco");
            public static By ProtocolosFornecidoEmpresa = By.ClassName("texto-protocolo-empresa-div");
            public static By DescricaoReclamacao = By.ClassName("texto-descricao-reclamacao-div");
            public static By PedidoEmpresa = By.ClassName("texto-pedido-reclamacao-div");
            public static By BotaoResponderReclamacao = By.CssSelector("#conteudo-decorator .btn.btn-sm.btn-info");

            public static By GridProtocolos = By.XPath("//*[@id=\"tbconsulta\"]/tbody");
            public static By DivConsultaInfo = By.Id("tbconsulta_info");
            public static By BotaoProximo = By.Id("tbconsulta_next");
        }

        public static class RegexProtocolo
        {
            public const string REGEX_NOME = @".+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_CPF = "CPF";
            public const string REGEX_CPF = @"CPF:.+[0-9_]";

            public const string PREFIXO_CEP = "CEP";
            public const string REGEX_CEP = @"CEP:.+[0-9_]";

            public const string PREFIXO_ENDERECO = "Endereço";
            public const string REGEX_ENDERECO = @"Endereço:.+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_TELEFONE = "Telefone";
            public const string REGEX_TELEFONE = @"Telefone:.+[0-9_]";

            public const string PREFIXO_CELULAR = "Celular";
            public const string REGEX_CELULAR = @"Celular:.+[0-9_]";

            public const string PREFIXO_EMAIL = "E-Mail";
            public const string REGEX_EMAIL = @"E-Mail:.+[a-zA-Z0-9_]";

            public const string PREFIXO_PROTOCOLO = "PROTOCOLO";
            public const string REGEX_PROTOCOLO = @"PROTOCOLO:.+[0-9_]";

            public const string PREFIXO_GESTOR = "GESTOR";
            public const string REGEX_GESTOR = @"GESTOR:.+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_DATA_ABERTURA = "Data de Abertura";
            public const string REGEX_DATA_ABERTURA = @"Data de Abertura:.+[0-9_]";

            public const string PREFIXO_COMO_CONTRATOU = "COMO COMPROU/CONTRATOU";
            public const string REGEX_COMO_CONTRATOU = @"COMO COMPROU/CONTRATOU:.+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_ASSUNTO = "ASSUNTO";
            public const string REGEX_ASSUNTO = @"ASSUNTO:.+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_AREA = "ÁREA";
            public const string REGEX_AREA = @"ÁREA:.+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_PROBLEMA = "PROBLEMA";
            public const string REGEX_PROBLEMA = @"PROBLEMA:.+[a-zA-zÀ-ÿ0-9/ [.,\/#!$%\^&\*;:{}=\-_`~()?<>]";

            public const string PREFIXO_SITUACAO = "SITUAÇÃO";
            public const string REGEX_SITUACAO = @"SITUAÇÃO:.+[a-zA-Z0-9_]";

            public const string PREFIXO_PRAZO_RESPOSTA = "Prazo para resposta do fornecedor";
            public const string REGEX_PRAZO_RESPOSTA = @"Prazo para resposta do fornecedor:.+[0-9_]";
        }
    }
}
