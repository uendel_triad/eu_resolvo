﻿using Btcc.EuResolvo.Dominio.Objetos;
using Btcc.Infraestrutura;
using Btcc.Infraestrutura.Extensoes;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using static Btcc.EuResolvo.Dominio.PageObjects.Constantes;

namespace Btcc.EuResolvo.Dominio.PageObjects
{
    public class FerramentaConsultaDetalhesProtocoloPage : PageBase<FerramentaConsultaDetalhesProtocoloPage>
    {
        public FerramentaConsultaDetalhesProtocoloPage(IWebDriver driver) : base(driver)
        {
        }

        protected override int TimeOutPadrao
        {
            get
            {
                return 2;
            }
        }

        protected int QtdDeItensPorPagina
        {
            get
            {
                return 100;
            }
        }

        public override FerramentaConsultaDetalhesProtocoloPage SelecioneItemEmComboPorTexto(By seletor, string texto, int timeOutParaElementoSerApresentado = 0)
        {
            var combo = new OpenQA.Selenium.Support.UI.SelectElement(Driver.FindElement(seletor, timeOutParaElementoSerApresentado, true));
            combo.SelectByText(texto);
            return this;
        }

        public override ReadOnlyCollection<IWebElement> ObtenhaElementos(By seletor, int timeOutParaElementoSerApresentado = 0, bool elementoVisivel = true)
        {
            return Driver.FindElements(seletor, timeOutParaElementoSerApresentado, elementoVisivel);
        }

        public void SelecioneQuantidadeDeRegistrosPorPagina()
        {
            SelecioneItemEmComboPorTexto(Constantes.ByComponentes.ComboConsultaQuantidade, QtdDeItensPorPagina.ToString(), 5);
        }

        public List<string> ConsulteListaDeNumerosDeProtocolos()
        {
            var listaDeNumeroProtocolos = new List<string>();
            IWebElement btnProximo;
            bool btnProximoHabilitado;
            int qtdDeItens = 1;

            do
            {
                AguardePesquisa(qtdDeItens);

                btnProximo = Driver.FindElement(ByComponentes.BotaoProximo);
                btnProximoHabilitado = !btnProximo.GetAttribute("class").Contains("ui-state-disabled");

                var tabela = Driver.FindElement(ByComponentes.GridProtocolos, 2, true);

                if (tabela != null)
                {
                    var linhas = tabela.FindElements(By.TagName("tr"));

                    for (var i = 0; i < linhas.Count; i++)
                    {
                        var colunas = linhas[i].FindElements(By.TagName("td"));

                        if (colunas.Count > 0)
                        {
                            var numeroProtocolo = colunas[0].Text;

                            if (!string.IsNullOrEmpty(numeroProtocolo))
                            {
                                listaDeNumeroProtocolos.Add(numeroProtocolo);
                            }
                        }
                    }
                }

                if (btnProximoHabilitado)
                    btnProximo.Click();

                qtdDeItens += QtdDeItensPorPagina;

            } while (btnProximoHabilitado);

            return listaDeNumeroProtocolos;
        }

        private void AguardePesquisa(int qtdDeItens)
        {
            var infoConsulta = string.Format("Mostrando de {0}", qtdDeItens);

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.TextToBePresentInElementLocated(ByComponentes.DivConsultaInfo, infoConsulta));
        }

        private void AguardeLinkDoProtocolo(string numeroProtocolo)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.LinkText(numeroProtocolo)));
        }

        private void AguardeDetalhesDoProtocolo()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(ByComponentes.BotaoResponderReclamacao));
        }

        public void PesquisePorDataAtualMenosUm()
        {
            CliqueViaJs(Constantes.ByComponentes.TogglePesquisar)
            .CliqueViaJs(Constantes.ByComponentes.BotaoLimpar)
            .EscrevaViaJs(Constantes.ByComponentes.CampoDataInicioPeriodo, DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"))
            .EscrevaViaJs(Constantes.ByComponentes.CampoHoraInicioPeriodo, "00:00:00")
            .EscrevaViaJs(Constantes.ByComponentes.CampoDataFimPeriodo, DateTime.Now.ToString("dd/MM/yyyy"))
            .EscrevaViaJs(Constantes.ByComponentes.CampoHoraFimPeriodo, DateTime.Now.ToString("HH:mm:ss"))
            .CliqueViaJs(Constantes.ByComponentes.BotaoPesquisar);
        }

        private void PesquisePorNumeroDeProtocolo(string numeroProtocolo)
        {
            CliqueViaJs(Constantes.ByComponentes.TogglePesquisar)
            .CliqueViaJs(Constantes.ByComponentes.BotaoLimpar)
            .EscrevaViaJs(Constantes.ByComponentes.CampoProtocolo, numeroProtocolo)
            .CliqueViaJs(Constantes.ByComponentes.BotaoPesquisar);
        }

        public List<Protocolo> ConsulteDadosDosProtocolos(string fornecedor, List<string> listaDeNumeroDeProtocolo)
        {
            var listaDeDadosDoProtocolo = new List<Protocolo>();

            listaDeNumeroDeProtocolo = new List<String>() { "2020.04/00002954914" };

            foreach (var numeroProtocolo in listaDeNumeroDeProtocolo)
            {
                SelecioneItemEmComboPorTexto(Constantes.ByComponentes.ComboFornecedorGerenciar, fornecedor);
                PesquisePorNumeroDeProtocolo(numeroProtocolo);

                AguardeLinkDoProtocolo(numeroProtocolo);

                CliqueEmElementoPorLinkText(numeroProtocolo);

                AguardeDetalhesDoProtocolo();

                var dadosDoProtocolo = new Protocolo();

                PreenchaDadosDoProtocolo(dadosDoProtocolo);
                //PreenchaDadosDoProtocolo(dadosDoProtocolo);

                SelecioneItemEmComboPorTexto(Constantes.ByComponentes.ComboFornecedorGerenciar, "Selecione");
            }

            return listaDeDadosDoProtocolo;
        }

        //private void PreenchaDadosDoProtocolo(DadosDoProtocolo dadosDoProtocolo)
        //{
        //    var informacoesDoProtocolo = ObtenhaElemento(ByComponentes.Protocolo);
        //    var linhas = informacoesDoProtocolo.FindElements(By.TagName("div"));

        //    if (linhas != null)
        //    {
        //        dadosDoProtocolo.Gestor = linhas.Count > 2 ? linhas[2].FindElement(By.TagName("span"))?.Text : string.Empty;
        //        dadosDoProtocolo.DataDeAbertura = linhas.Count > 3 ? linhas[3].FindElement(By.TagName("span"))?.Text : string.Empty;
        //        dadosDoProtocolo.ComoContratou = linhas.Count > 4 ? linhas[4].FindElements(By.TagName("span"))?[0].Text : string.Empty;
        //        dadosDoProtocolo.Assunto = linhas.Count > 4 ? linhas[4].FindElements(By.TagName("span"))?[1].Text : string.Empty;
        //        dadosDoProtocolo.Area = linhas.Count > 5 ? linhas[5].FindElements(By.TagName("span"))?[0].Text : string.Empty;
        //        dadosDoProtocolo.Problema = linhas.Count > 5 ? linhas[5].FindElements(By.TagName("span"))?[1].Text : string.Empty;
        //    }

        //    dadosDoProtocolo.Situacao = Driver.FindElement(ByComponentes.Situacao)?.Text.Replace("SITUAÇÃO: ", string.Empty);
        //    dadosDoProtocolo.PrazoParaRespostaDoFornecedor = Driver.FindElement(ByComponentes.PrazoRespostaFornecedor)?.Text.Replace("Prazo para resposta do fornecedor: ", string.Empty);
        //    dadosDoProtocolo.ProtocolosFornecidos = Driver.FindElement(ByComponentes.ProtocolosFornecidos)?.Text;

        //}

        private void PreenchaDadosDoProtocolo(Protocolo dadosDoProtocolo)
        {
            var elInformacoesDoProtocolo = ObtenhaElemento(ByComponentes.InformacoesDoProtocolo);
            var dadosDoProtocoloWeb = elInformacoesDoProtocolo.Text.Trim();

            var rgNome = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_NOME);
            if (rgNome.Success)
                dadosDoProtocolo.Nome = rgNome.Value.Trim();

            var rgCpf = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_CPF);
            if (rgCpf.Success)
                dadosDoProtocolo.Cpf = FiltreValorDoRegex(rgCpf, RegexProtocolo.PREFIXO_CPF);

            var rgCep = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_CEP);
            if (rgCep.Success)
                dadosDoProtocolo.Cep = FiltreValorDoRegex(rgCep, RegexProtocolo.PREFIXO_CEP);

            var rgEndereco = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_ENDERECO);
            if (rgEndereco.Success)
                dadosDoProtocolo.Endereco = FiltreValorDoRegex(rgEndereco, RegexProtocolo.PREFIXO_ENDERECO);

            var rgTelefone = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_TELEFONE);
            if (rgTelefone.Success)
                dadosDoProtocolo.Telefone = FiltreValorDoRegex(rgTelefone, RegexProtocolo.PREFIXO_TELEFONE);

            var rgCelular = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_CELULAR);
            if (rgCelular.Success)
                dadosDoProtocolo.Celular = FiltreValorDoRegex(rgCelular, RegexProtocolo.PREFIXO_CELULAR);

            var rgEmail = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_EMAIL);
            if (rgEmail.Success)
                dadosDoProtocolo.Email = FiltreValorDoRegex(rgEmail, RegexProtocolo.PREFIXO_EMAIL);

            var rgGestor = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_GESTOR);
            if (rgGestor.Success)
                dadosDoProtocolo.Gestor = FiltreValorDoRegex(rgGestor, RegexProtocolo.PREFIXO_GESTOR);

            var rgDataDeAbertura = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_DATA_ABERTURA);
            if (rgDataDeAbertura.Success)
                dadosDoProtocolo.DataDeAbertura = FiltreValorDoRegex(rgDataDeAbertura, RegexProtocolo.PREFIXO_DATA_ABERTURA);

            var rgComoContratou = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_COMO_CONTRATOU);
            if (rgComoContratou.Success)
                dadosDoProtocolo.ComoContratou = FiltreValorDoRegex(rgComoContratou, RegexProtocolo.PREFIXO_COMO_CONTRATOU);

            var rgAssunto = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_ASSUNTO);
            if (rgAssunto.Success)
                dadosDoProtocolo.Assunto = FiltreValorDoRegex(rgAssunto, RegexProtocolo.PREFIXO_ASSUNTO);

            var rgArea = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_AREA);
            if (rgArea.Success)
                dadosDoProtocolo.Area = FiltreValorDoRegex(rgArea, RegexProtocolo.PREFIXO_AREA);

            var rgProblema = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_PROBLEMA);
            if (rgProblema.Success)
                dadosDoProtocolo.Problema = FiltreValorDoRegex(rgProblema, RegexProtocolo.PREFIXO_PROBLEMA);

            var rgSituacao = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_SITUACAO);
            if (rgSituacao.Success)
                dadosDoProtocolo.Situacao = FiltreValorDoRegex(rgSituacao, RegexProtocolo.PREFIXO_SITUACAO);

            var rgPrazoParaResposta = Regex.Match(dadosDoProtocoloWeb, RegexProtocolo.REGEX_PRAZO_RESPOSTA);
            if (rgPrazoParaResposta.Success)
                dadosDoProtocolo.PrazoParaRespostaDoFornecedor = FiltreValorDoRegex(rgPrazoParaResposta, RegexProtocolo.PREFIXO_PRAZO_RESPOSTA);

            dadosDoProtocolo.ProtocolosFornecidos = elInformacoesDoProtocolo.FindElement(ByComponentes.ProtocolosFornecidoEmpresa).Text.Trim();
            dadosDoProtocolo.DescricaoDaReclamacao = elInformacoesDoProtocolo.FindElement(ByComponentes.DescricaoReclamacao).Text.Trim();
            dadosDoProtocolo.PedidoAEmpresa = elInformacoesDoProtocolo.FindElement(ByComponentes.PedidoEmpresa).Text.Trim();


        }

        private string FiltreValorDoRegex(Match regex, string prefixo)
        {
            return regex.Value.Replace(":", string.Empty).Replace(prefixo, string.Empty).Trim();
        }

        //private void PreenchaDadosPessoaisDoConsumidor(DadosDoProtocolo dadosDoProtocolo)
        //{
        //    var info = ObtenhaElemento(By.ClassName("fundo-branco"));
        //    var informacoesDoConsumidor = ObtenhaElemento(ByComponentes.Consumidor);

        //    dadosDoProtocolo.Nome = informacoesDoConsumidor.FindElement(ByComponentes.NomeUsuario).Text;

        //    var tabelaDadosDoConsumidor = informacoesDoConsumidor.FindElement(ByComponentes.DadosDoUsuario);

        //    if (tabelaDadosDoConsumidor != null)
        //    {
        //        var linhas = tabelaDadosDoConsumidor.FindElements(By.TagName("tr"));

        //        dadosDoProtocolo.Cpf = ObtenhaValor(linhas, 0, 0);
        //        dadosDoProtocolo.Cep = ObtenhaValor(linhas, 1, 0);
        //        dadosDoProtocolo.Endereco = ObtenhaValor(linhas, 2, 0);
        //        dadosDoProtocolo.Telefone = ObtenhaValor(linhas, 3, 0);
        //        dadosDoProtocolo.Celular = ObtenhaValor(linhas, 4, 0);
        //        dadosDoProtocolo.Email = ObtenhaValor(linhas, 5, 0);
        //    }
        //}

        private string ObtenhaValor(ReadOnlyCollection<IWebElement> linhas, int indiceLinha, int indiceColuna)
        {
            if (linhas != null && linhas.Count > indiceLinha && linhas[indiceLinha] != null)
            {
                var colunas = linhas[indiceLinha].FindElements(By.TagName("td"));

                if (colunas != null && colunas.Count > indiceColuna)
                {
                    return colunas[indiceColuna].Text;
                }
            }

            return string.Empty;
        }
    }
}
