﻿namespace Btcc.EuResolvo.Dominio.Constantes
{
    public static class CamposProtocolo
    {
        public const string CPF = "CPF:";

        public const string CEP = "CEP:";

        public const string ENDERECO = "Endereço:";

        public const string TELEFONE = "Telefone:";

        public const string CELULAR = "Celular:";

        public const string EMAIL = "E-Mail:";

        public const string PROTOCOLO = "PROTOCOLO";

        public const string GESTOR = "GESTOR";

        public const string DATA_ABERTURA = "Data de Abertura";

        public const string COMO_CONTRATOU = "COMO COMPROU/CONTRATOU";

        public const string ASSUNTO = "ASSUNTO";

        public const string AREA = "ÁREA";

        public const string PROBLEMA = "PROBLEMA";

        public const string SITUACAO = "SITUAÇÃO";

        public const string PRAZO_RESPOSTA = "Prazo para resposta do fornecedor";
    }
}
