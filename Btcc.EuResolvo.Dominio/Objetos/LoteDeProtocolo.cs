﻿namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class LoteDeProtocolo
    {
        public Validacoes Validacoes { get; set; }

        public ListaDeProtocolo Dados { get; set; }
    }
}
