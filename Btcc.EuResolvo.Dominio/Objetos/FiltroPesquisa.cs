﻿using System;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class FiltroPesquisa
    {
        public FiltroPesquisa(DateTime dataInicio, DateTime dataFim)
        {
            DataInicio = dataInicio;
            DataFim = dataFim;
        }

        public DateTime DataInicio { get; set; }

        public DateTime DataFim { get; set; }
    }
}
