﻿using System;
using System.Collections.Generic;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class Anotacao
    {
        public Anotacao()
        {
            Detalhes = new List<Detalhe>();
            Anexos = new List<Anexo>();
        }

        public DateTime Data { get; set; }

        public string Descricao { get; set; }

        public string Autor { get; set; }

        public List<Detalhe> Detalhes { get; set; }

        public List<Anexo> Anexos { get; set; }
    }
}
