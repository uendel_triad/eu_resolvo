﻿using System;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class Anexo
    {
        public int Chave { get; set; }

        public string Nome { get; set; }

        public string Data { get; set; }

        public DateTime? DataFormatada
        {
            get
            {
                if (!string.IsNullOrEmpty(Data))
                    return DateTime.ParseExact(Data, "dd/MM/yyyy", null);

                return null;
            }
        }

        public string Autor { get; set; }

        public byte[] Conteudo { get; set; }
    }
}
