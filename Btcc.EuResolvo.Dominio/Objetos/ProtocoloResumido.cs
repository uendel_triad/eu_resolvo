﻿namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class ProtocoloResumido
    {
        public string NumeroProtocolo { get; set; }

        public string Id { get; set; }

        public string NomeReclamante { get; set; }

        public string NomeProcon { get; set; }

        public long? DataAbertura { get; set; }

        public long? DataPrazo { get; set; }

        public long? UltimoComplementoConsumidor { get; set; }

        public int? CodigoSituacao { get; set; }

        public string Situacao { get; set; }

        public string CpfReclamante { get; set; }

        public string Area { get; set; }

        public string Assunto { get; set; }

        public string Problema { get; set; }

        public string Avaliacao { get; set; }

        public string CnpjFornecedor { get; set; }

        public string Rclm { get; set; }

        public string TextoReferenciaFornecedor { get; set; }

        public string NumeroProtocoloFormatado { get; set; }

        public string NomeFantasiaOuRazaoSocial { get; set; }

        public int? CodigoSituacaoInt { get; set; }

        public string NomeFornecedor { get; set; }

        public string DataPrazoFormatada { get; set; }

        public string DataAberturaFormatada { get; set; }

        public string NomeFantasiaOuRazaoSocialEscape { get; set; }

        public string NomeReclamanteEscape { get; set; }

        public string NomeProconEscape { get; set; }

        public int? Rnum { get; set; }

        public string HoraAberturaFormatada { get; set; }

        public long? CpfReclamanteSemFormatacao { get; set; }

        public long? CnpjFornecedorSemFormatacao { get; set; }

        public string UltimoComplementoConsumidorFormatado { get; set; }
    }
}
