﻿namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class Fornecedor
    {
        public string Value { get; set; }

        public string Label { get; set; }
    }
}
