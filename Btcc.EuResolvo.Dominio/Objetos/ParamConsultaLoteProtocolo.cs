﻿using System;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class ParamConsultaLoteProtocolo
    {
        public ParamConsultaLoteProtocolo(int contador, int indiceInicial, int quantidadesDeItens, DateTime dataIniPeriodo, DateTime dataFimPeriodo)
        {
            Contador = contador;
            IndiceInicial = indiceInicial;
            QuantidadesDeItens = quantidadesDeItens;
            DataIniPeriodo = dataIniPeriodo;
            DataFimPeriodo = dataFimPeriodo;
        }

        public int Contador { get; set; }

        public int IndiceInicial { get; set; }

        public int QuantidadesDeItens { get; set; }

        public DateTime DataIniPeriodo { get; set; }

        public DateTime DataFimPeriodo { get; set; }
    }
}
