﻿using Btcc.Infraestrutura.Extensoes;
using System.Collections.Generic;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class Protocolo
    {
        private string _celular;

        private string _cep;

        private string _cpf;

        private string _telefone;

        public Protocolo()
        {
            ListaDeAnotacao = new List<Anotacao>();
        }

        public string Area { get; set; }

        public string Assunto { get; set; }

        public string Celular
        {
            get
            {
                return _celular;
            }
            set
            {
                _celular = value.RemovaEspacosECaracteres();
            }
        }

        public string Cep
        {
            get
            {
                return _cep;
            }
            set
            {
                _cep = value.RemovaEspacosECaracteres();
            }
        }

        public string ComoContratou { get; set; }

        public string Cpf
        {
            get
            {
                return _cpf;
            }
            set
            {
                _cpf = value.RemovaEspacosECaracteres();
            }
        }

        public string DataDeAbertura { get; set; }

        public string DescricaoDaReclamacao { get; set; }

        public string Email { get; set; }

        public string Endereco { get; set; }

        public string Gestor { get; set; }

        public string InformacoesAdicionais { get; set; }

        public List<Anotacao> ListaDeAnotacao { get; set; }

        public string Nome { get; set; }

        public string NumeroProtocolo { get; set; }

        public string PedidoAEmpresa { get; set; }

        public string PrazoParaRespostaDoFornecedor { get; set; }

        public string Problema { get; set; }

        public string ProtocolosFornecidos { get; set; }

        public string Situacao { get; set; }

        public string Telefone
        {
            get
            {
                return _telefone;
            }
            set
            {
                _telefone = value.RemovaEspacosECaracteres();
            }
        }
    }
}