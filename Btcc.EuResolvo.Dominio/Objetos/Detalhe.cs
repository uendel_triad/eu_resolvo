﻿using System;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class Detalhe
    {
        public int Codigo { get; set; }

        public string InteracaoGestor { get; set; }

        public bool TipoTramiteAbertura { get; set; }

        public string Autor { get; set; }

        public string Tipo { get; set; }

        public string Texto { get; set; }

        public string Criacao { get; set; }

        public DateTime? DataCriacao
        {
            get
            {
                if (!string.IsNullOrEmpty(Criacao))
                    return DateTime.ParseExact(Criacao, "dd/MM/yyyy HH:mm:ss", null);

                return null;
            }
        }

        public bool TipoTramiteAvalicaoReclamacao { get; set; }

        public bool TipoTramiteEdicaoReclamacao { get; set; }

        public bool PodeVisualizarDetalhes { get; set; }

        public bool PodeVisualizarAnexo { get; set; }

        public bool ConsumidorPodeVisualizar { get; set; }
    }
}
