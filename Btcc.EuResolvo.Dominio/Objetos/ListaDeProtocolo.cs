﻿using System.Collections.Generic;

namespace Btcc.EuResolvo.Dominio.Objetos
{
    public class ListaDeProtocolo
    {
        public List<ProtocoloResumido> AaData { get; set; }

        public int ITotalRecords { get; set; }

        public int ITotalDisplayRecords { get; set; }

        public int SEcho { get; set; }
    }
}
