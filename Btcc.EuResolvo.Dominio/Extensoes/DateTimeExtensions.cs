﻿namespace System
{
    public static class DateTimeExtensions
    {
        public static DateTime HorarioDeBrasilia(this DateTime data)
        {
            TimeZoneInfo horaBrasilia = TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time");

            return TimeZoneInfo.ConvertTimeFromUtc(data, horaBrasilia);
        }
    }
}
