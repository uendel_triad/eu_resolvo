﻿definaNamespace('btcc.template');

btcc.template = function ($el, opcoes) {
    "use strict";
    if (!$el)
        throw "O Item não foi definido.";

    this.$el = $el;
    this.el = $el[0];
    this.opcoes = opcoes != null ? opcoes : {};

    this.$el.data("btcc.template", this);

    this._inicialize();
};

btcc.template.prototype = {
    _inicialize: function () {
        this._preparaComponente();
        this._ligaEventos();
    },

    _preparaComponente: function () {
        var _this = this;

    },

    _ligaEventos: function () {
        var _this = this;

    }
};