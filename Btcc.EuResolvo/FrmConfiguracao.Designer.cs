﻿namespace Btcc.EuResolvo
{
    partial class FrmConfiguracao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEuResolvoLogin = new MetroFramework.Controls.MetroLabel();
            this.groupBoxEuResolvo = new System.Windows.Forms.GroupBox();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox2 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblEuResolvoModoAutomatico = new System.Windows.Forms.TextBox();
            this.txtEuResolvoModoAutomatico = new System.Windows.Forms.CheckBox();
            this.lblEuResolvoDias = new MetroFramework.Controls.MetroLabel();
            this.lblEuResolvoPeriodoRelatorio = new MetroFramework.Controls.MetroLabel();
            this.txtEuResolvoPeriodoRelatorio = new System.Windows.Forms.NumericUpDown();
            this.txtEuResolvoSenha = new MetroFramework.Controls.MetroTextBox();
            this.txtEuResolvoLogin = new MetroFramework.Controls.MetroTextBox();
            this.lblEuResolvoSenha = new MetroFramework.Controls.MetroLabel();
            this.lblPortalEuResolvoLink = new System.Windows.Forms.LinkLabel();
            this.btnSalvar = new MetroFramework.Controls.MetroButton();
            this.btnCancelar = new MetroFramework.Controls.MetroButton();
            this.groupBoxFtp = new System.Windows.Forms.GroupBox();
            this.lblFtpPasta = new MetroFramework.Controls.MetroLabel();
            this.txtFtpPasta = new MetroFramework.Controls.MetroTextBox();
            this.lblFtpHost = new MetroFramework.Controls.MetroLabel();
            this.txtFtpHost = new MetroFramework.Controls.MetroTextBox();
            this.txtFtpAnonimo = new System.Windows.Forms.CheckBox();
            this.lblFtpPorta = new MetroFramework.Controls.MetroLabel();
            this.txtFtpPorta = new MetroFramework.Controls.MetroTextBox();
            this.lblFtpSenha = new MetroFramework.Controls.MetroLabel();
            this.txtFtpSenha = new MetroFramework.Controls.MetroTextBox();
            this.lblFtpLogin = new MetroFramework.Controls.MetroLabel();
            this.txtFtpLogin = new MetroFramework.Controls.MetroTextBox();
            this.groupBoxEuResolvo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEuResolvoPeriodoRelatorio)).BeginInit();
            this.groupBoxFtp.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEuResolvoLogin
            // 
            this.lblEuResolvoLogin.AutoSize = true;
            this.lblEuResolvoLogin.Location = new System.Drawing.Point(10, 16);
            this.lblEuResolvoLogin.Name = "lblEuResolvoLogin";
            this.lblEuResolvoLogin.Size = new System.Drawing.Size(41, 19);
            this.lblEuResolvoLogin.TabIndex = 0;
            this.lblEuResolvoLogin.Text = "Login";
            // 
            // groupBoxEuResolvo
            // 
            this.groupBoxEuResolvo.Controls.Add(this.metroTextBox1);
            this.groupBoxEuResolvo.Controls.Add(this.metroTextBox2);
            this.groupBoxEuResolvo.Controls.Add(this.metroLabel1);
            this.groupBoxEuResolvo.Controls.Add(this.metroLabel2);
            this.groupBoxEuResolvo.Controls.Add(this.lblEuResolvoModoAutomatico);
            this.groupBoxEuResolvo.Controls.Add(this.txtEuResolvoModoAutomatico);
            this.groupBoxEuResolvo.Controls.Add(this.lblEuResolvoDias);
            this.groupBoxEuResolvo.Controls.Add(this.lblEuResolvoPeriodoRelatorio);
            this.groupBoxEuResolvo.Controls.Add(this.txtEuResolvoPeriodoRelatorio);
            this.groupBoxEuResolvo.Controls.Add(this.txtEuResolvoSenha);
            this.groupBoxEuResolvo.Controls.Add(this.txtEuResolvoLogin);
            this.groupBoxEuResolvo.Controls.Add(this.lblEuResolvoSenha);
            this.groupBoxEuResolvo.Controls.Add(this.lblEuResolvoLogin);
            this.groupBoxEuResolvo.Location = new System.Drawing.Point(13, 62);
            this.groupBoxEuResolvo.Name = "groupBoxEuResolvo";
            this.groupBoxEuResolvo.Size = new System.Drawing.Size(448, 145);
            this.groupBoxEuResolvo.TabIndex = 1;
            this.groupBoxEuResolvo.TabStop = false;
            this.groupBoxEuResolvo.Text = "Eu Resolvo";
            this.groupBoxEuResolvo.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // metroTextBox1
            // 
            this.metroTextBox1.Location = new System.Drawing.Point(233, 195);
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '●';
            this.metroTextBox1.Size = new System.Drawing.Size(203, 23);
            this.metroTextBox1.TabIndex = 13;
            this.metroTextBox1.UseSystemPasswordChar = true;
            // 
            // metroTextBox2
            // 
            this.metroTextBox2.Location = new System.Drawing.Point(16, 195);
            this.metroTextBox2.Name = "metroTextBox2";
            this.metroTextBox2.Size = new System.Drawing.Size(203, 23);
            this.metroTextBox2.TabIndex = 12;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(229, 173);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(44, 19);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "Senha";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(12, 173);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(41, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Login";
            // 
            // lblEuResolvoModoAutomatico
            // 
            this.lblEuResolvoModoAutomatico.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblEuResolvoModoAutomatico.Location = new System.Drawing.Point(14, 125);
            this.lblEuResolvoModoAutomatico.Name = "lblEuResolvoModoAutomatico";
            this.lblEuResolvoModoAutomatico.Size = new System.Drawing.Size(420, 13);
            this.lblEuResolvoModoAutomatico.TabIndex = 9;
            this.lblEuResolvoModoAutomatico.Text = "*Neste modo, a aplicação será executada e finalizada sem apresentar a tela princi" +
    "pal.";
            this.lblEuResolvoModoAutomatico.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtEuResolvoModoAutomatico
            // 
            this.txtEuResolvoModoAutomatico.AutoSize = true;
            this.txtEuResolvoModoAutomatico.Location = new System.Drawing.Point(14, 101);
            this.txtEuResolvoModoAutomatico.Name = "txtEuResolvoModoAutomatico";
            this.txtEuResolvoModoAutomatico.Size = new System.Drawing.Size(148, 17);
            this.txtEuResolvoModoAutomatico.TabIndex = 8;
            this.txtEuResolvoModoAutomatico.Text = "Modo automático (batch)*";
            this.txtEuResolvoModoAutomatico.UseVisualStyleBackColor = true;
            this.txtEuResolvoModoAutomatico.CheckedChanged += new System.EventHandler(this.txtEuResolvoModoAutomatico_CheckedChanged);
            // 
            // lblEuResolvoDias
            // 
            this.lblEuResolvoDias.AutoSize = true;
            this.lblEuResolvoDias.Location = new System.Drawing.Point(273, 72);
            this.lblEuResolvoDias.Name = "lblEuResolvoDias";
            this.lblEuResolvoDias.Size = new System.Drawing.Size(40, 19);
            this.lblEuResolvoDias.TabIndex = 6;
            this.lblEuResolvoDias.Text = "dia(s)";
            // 
            // lblEuResolvoPeriodoRelatorio
            // 
            this.lblEuResolvoPeriodoRelatorio.AutoSize = true;
            this.lblEuResolvoPeriodoRelatorio.Location = new System.Drawing.Point(10, 72);
            this.lblEuResolvoPeriodoRelatorio.Name = "lblEuResolvoPeriodoRelatorio";
            this.lblEuResolvoPeriodoRelatorio.Size = new System.Drawing.Size(132, 19);
            this.lblEuResolvoPeriodoRelatorio.TabIndex = 5;
            this.lblEuResolvoPeriodoRelatorio.Text = "Período do relatório:";
            this.lblEuResolvoPeriodoRelatorio.Click += new System.EventHandler(this.metroLabel2_Click);
            // 
            // txtEuResolvoPeriodoRelatorio
            // 
            this.txtEuResolvoPeriodoRelatorio.Location = new System.Drawing.Point(231, 72);
            this.txtEuResolvoPeriodoRelatorio.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.txtEuResolvoPeriodoRelatorio.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtEuResolvoPeriodoRelatorio.Name = "txtEuResolvoPeriodoRelatorio";
            this.txtEuResolvoPeriodoRelatorio.Size = new System.Drawing.Size(40, 20);
            this.txtEuResolvoPeriodoRelatorio.TabIndex = 4;
            this.txtEuResolvoPeriodoRelatorio.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtEuResolvoPeriodoRelatorio.ValueChanged += new System.EventHandler(this.txtPeriodoRelatorio_ValueChanged);
            // 
            // txtEuResolvoSenha
            // 
            this.txtEuResolvoSenha.BackColor = System.Drawing.Color.White;
            this.txtEuResolvoSenha.CustomBackground = true;
            this.txtEuResolvoSenha.Location = new System.Drawing.Point(231, 38);
            this.txtEuResolvoSenha.Name = "txtEuResolvoSenha";
            this.txtEuResolvoSenha.PasswordChar = '●';
            this.txtEuResolvoSenha.Size = new System.Drawing.Size(203, 23);
            this.txtEuResolvoSenha.TabIndex = 3;
            this.txtEuResolvoSenha.UseSystemPasswordChar = true;
            this.txtEuResolvoSenha.Click += new System.EventHandler(this.txtSenhaConsumidorGov_Click);
            this.txtEuResolvoSenha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEuResolvoSenha_KeyDown);
            // 
            // txtEuResolvoLogin
            // 
            this.txtEuResolvoLogin.BackColor = System.Drawing.Color.White;
            this.txtEuResolvoLogin.CustomBackground = true;
            this.txtEuResolvoLogin.Location = new System.Drawing.Point(14, 38);
            this.txtEuResolvoLogin.Name = "txtEuResolvoLogin";
            this.txtEuResolvoLogin.Size = new System.Drawing.Size(203, 23);
            this.txtEuResolvoLogin.TabIndex = 2;
            this.txtEuResolvoLogin.Click += new System.EventHandler(this.txtEuResolvoLogin_Click);
            // 
            // lblEuResolvoSenha
            // 
            this.lblEuResolvoSenha.AutoSize = true;
            this.lblEuResolvoSenha.Location = new System.Drawing.Point(227, 16);
            this.lblEuResolvoSenha.Name = "lblEuResolvoSenha";
            this.lblEuResolvoSenha.Size = new System.Drawing.Size(44, 19);
            this.lblEuResolvoSenha.TabIndex = 1;
            this.lblEuResolvoSenha.Text = "Senha";
            // 
            // lblPortalEuResolvoLink
            // 
            this.lblPortalEuResolvoLink.AutoSize = true;
            this.lblPortalEuResolvoLink.Location = new System.Drawing.Point(405, 33);
            this.lblPortalEuResolvoLink.Name = "lblPortalEuResolvoLink";
            this.lblPortalEuResolvoLink.Size = new System.Drawing.Size(56, 13);
            this.lblPortalEuResolvoLink.TabIndex = 7;
            this.lblPortalEuResolvoLink.TabStop = true;
            this.lblPortalEuResolvoLink.Text = "Link portal";
            this.lblPortalEuResolvoLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPortalEuResolvoLink_LinkClicked);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(244, 361);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(80, 30);
            this.btnSalvar.Style = MetroFramework.MetroColorStyle.Green;
            this.btnSalvar.TabIndex = 4;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(150, 361);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(80, 30);
            this.btnCancelar.Style = MetroFramework.MetroColorStyle.Red;
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupBoxFtp
            // 
            this.groupBoxFtp.Controls.Add(this.lblFtpPasta);
            this.groupBoxFtp.Controls.Add(this.txtFtpPasta);
            this.groupBoxFtp.Controls.Add(this.lblFtpHost);
            this.groupBoxFtp.Controls.Add(this.txtFtpHost);
            this.groupBoxFtp.Controls.Add(this.txtFtpAnonimo);
            this.groupBoxFtp.Controls.Add(this.lblFtpPorta);
            this.groupBoxFtp.Controls.Add(this.txtFtpPorta);
            this.groupBoxFtp.Controls.Add(this.lblFtpSenha);
            this.groupBoxFtp.Controls.Add(this.txtFtpSenha);
            this.groupBoxFtp.Controls.Add(this.lblFtpLogin);
            this.groupBoxFtp.Controls.Add(this.txtFtpLogin);
            this.groupBoxFtp.Location = new System.Drawing.Point(13, 213);
            this.groupBoxFtp.Name = "groupBoxFtp";
            this.groupBoxFtp.Size = new System.Drawing.Size(448, 142);
            this.groupBoxFtp.TabIndex = 8;
            this.groupBoxFtp.TabStop = false;
            this.groupBoxFtp.Text = "FTP";
            // 
            // lblFtpPasta
            // 
            this.lblFtpPasta.AutoSize = true;
            this.lblFtpPasta.Location = new System.Drawing.Point(227, 16);
            this.lblFtpPasta.Name = "lblFtpPasta";
            this.lblFtpPasta.Size = new System.Drawing.Size(39, 19);
            this.lblFtpPasta.TabIndex = 14;
            this.lblFtpPasta.Text = "Pasta";
            // 
            // txtFtpPasta
            // 
            this.txtFtpPasta.BackColor = System.Drawing.Color.White;
            this.txtFtpPasta.CustomBackground = true;
            this.txtFtpPasta.Location = new System.Drawing.Point(231, 38);
            this.txtFtpPasta.Name = "txtFtpPasta";
            this.txtFtpPasta.Size = new System.Drawing.Size(203, 23);
            this.txtFtpPasta.TabIndex = 14;
            // 
            // lblFtpHost
            // 
            this.lblFtpHost.AutoSize = true;
            this.lblFtpHost.Location = new System.Drawing.Point(10, 16);
            this.lblFtpHost.Name = "lblFtpHost";
            this.lblFtpHost.Size = new System.Drawing.Size(35, 19);
            this.lblFtpHost.TabIndex = 14;
            this.lblFtpHost.Text = "Host";
            // 
            // txtFtpHost
            // 
            this.txtFtpHost.BackColor = System.Drawing.Color.White;
            this.txtFtpHost.CustomBackground = true;
            this.txtFtpHost.Location = new System.Drawing.Point(14, 38);
            this.txtFtpHost.Name = "txtFtpHost";
            this.txtFtpHost.Size = new System.Drawing.Size(203, 23);
            this.txtFtpHost.TabIndex = 14;
            // 
            // txtFtpAnonimo
            // 
            this.txtFtpAnonimo.AutoSize = true;
            this.txtFtpAnonimo.Location = new System.Drawing.Point(14, 121);
            this.txtFtpAnonimo.Name = "txtFtpAnonimo";
            this.txtFtpAnonimo.Size = new System.Drawing.Size(67, 17);
            this.txtFtpAnonimo.TabIndex = 14;
            this.txtFtpAnonimo.Text = "Anônimo";
            this.txtFtpAnonimo.UseVisualStyleBackColor = true;
            this.txtFtpAnonimo.CheckedChanged += new System.EventHandler(this.txtFtpAnonimo_CheckedChanged);
            // 
            // lblFtpPorta
            // 
            this.lblFtpPorta.AutoSize = true;
            this.lblFtpPorta.Location = new System.Drawing.Point(380, 64);
            this.lblFtpPorta.Name = "lblFtpPorta";
            this.lblFtpPorta.Size = new System.Drawing.Size(41, 19);
            this.lblFtpPorta.TabIndex = 16;
            this.lblFtpPorta.Text = "Porta";
            // 
            // txtFtpPorta
            // 
            this.txtFtpPorta.BackColor = System.Drawing.Color.White;
            this.txtFtpPorta.CustomBackground = true;
            this.txtFtpPorta.Location = new System.Drawing.Point(384, 86);
            this.txtFtpPorta.Name = "txtFtpPorta";
            this.txtFtpPorta.Size = new System.Drawing.Size(50, 23);
            this.txtFtpPorta.TabIndex = 15;
            // 
            // lblFtpSenha
            // 
            this.lblFtpSenha.AutoSize = true;
            this.lblFtpSenha.Location = new System.Drawing.Point(195, 64);
            this.lblFtpSenha.Name = "lblFtpSenha";
            this.lblFtpSenha.Size = new System.Drawing.Size(44, 19);
            this.lblFtpSenha.TabIndex = 14;
            this.lblFtpSenha.Text = "Senha";
            // 
            // txtFtpSenha
            // 
            this.txtFtpSenha.BackColor = System.Drawing.Color.White;
            this.txtFtpSenha.CustomBackground = true;
            this.txtFtpSenha.Location = new System.Drawing.Point(199, 86);
            this.txtFtpSenha.Name = "txtFtpSenha";
            this.txtFtpSenha.PasswordChar = '●';
            this.txtFtpSenha.Size = new System.Drawing.Size(171, 23);
            this.txtFtpSenha.TabIndex = 14;
            this.txtFtpSenha.UseSystemPasswordChar = true;
            // 
            // lblFtpLogin
            // 
            this.lblFtpLogin.AutoSize = true;
            this.lblFtpLogin.Location = new System.Drawing.Point(10, 64);
            this.lblFtpLogin.Name = "lblFtpLogin";
            this.lblFtpLogin.Size = new System.Drawing.Size(41, 19);
            this.lblFtpLogin.TabIndex = 14;
            this.lblFtpLogin.Text = "Login";
            // 
            // txtFtpLogin
            // 
            this.txtFtpLogin.BackColor = System.Drawing.Color.White;
            this.txtFtpLogin.CustomBackground = true;
            this.txtFtpLogin.Location = new System.Drawing.Point(14, 86);
            this.txtFtpLogin.Name = "txtFtpLogin";
            this.txtFtpLogin.Size = new System.Drawing.Size(171, 23);
            this.txtFtpLogin.TabIndex = 14;
            // 
            // FrmConfiguracao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 397);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.lblPortalEuResolvoLink);
            this.Controls.Add(this.groupBoxEuResolvo);
            this.Controls.Add(this.groupBoxFtp);
            this.Name = "FrmConfiguracao";
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Configurações";
            this.Load += new System.EventHandler(this.FrmConfiguracao_Load);
            this.groupBoxEuResolvo.ResumeLayout(false);
            this.groupBoxEuResolvo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEuResolvoPeriodoRelatorio)).EndInit();
            this.groupBoxFtp.ResumeLayout(false);
            this.groupBoxFtp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblEuResolvoLogin;
        private System.Windows.Forms.GroupBox groupBoxEuResolvo;
        private MetroFramework.Controls.MetroTextBox txtEuResolvoSenha;
        private MetroFramework.Controls.MetroTextBox txtEuResolvoLogin;
        private MetroFramework.Controls.MetroLabel lblEuResolvoSenha;
        private MetroFramework.Controls.MetroButton btnSalvar;
        private MetroFramework.Controls.MetroButton btnCancelar;
        private MetroFramework.Controls.MetroLabel lblEuResolvoPeriodoRelatorio;
        private System.Windows.Forms.NumericUpDown txtEuResolvoPeriodoRelatorio;
        private MetroFramework.Controls.MetroLabel lblEuResolvoDias;
        private System.Windows.Forms.LinkLabel lblPortalEuResolvoLink;
        private System.Windows.Forms.TextBox lblEuResolvoModoAutomatico;
        private System.Windows.Forms.CheckBox txtEuResolvoModoAutomatico;
        private System.Windows.Forms.GroupBox groupBoxFtp;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroTextBox metroTextBox2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblFtpLogin;
        private MetroFramework.Controls.MetroTextBox txtFtpLogin;
        private MetroFramework.Controls.MetroLabel lblFtpPorta;
        private MetroFramework.Controls.MetroTextBox txtFtpPorta;
        private MetroFramework.Controls.MetroLabel lblFtpSenha;
        private MetroFramework.Controls.MetroTextBox txtFtpSenha;
        private System.Windows.Forms.CheckBox txtFtpAnonimo;
        private MetroFramework.Controls.MetroLabel lblFtpPasta;
        private MetroFramework.Controls.MetroTextBox txtFtpPasta;
        private MetroFramework.Controls.MetroLabel lblFtpHost;
        private MetroFramework.Controls.MetroTextBox txtFtpHost;
    }
}