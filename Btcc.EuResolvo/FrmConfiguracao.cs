﻿using Btcc.Infraestrutura.Util;
using Btcc.Infraestrutura.WindowsForms;
using MetroFramework.Forms;
using System;
using System.Windows.Forms;

namespace Btcc.EuResolvo
{
    public partial class FrmConfiguracao : MetroForm
    {
        public FrmConfiguracao()
        {
            InitializeComponent();
            InicializeDadosSalvos();
        }

        private void InicializeDadosSalvos()
        {
            var euResolvoLogin = DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoLogin", true);
            if (!string.IsNullOrEmpty(euResolvoLogin))
            {
                // Eu Resolvo
                txtEuResolvoLogin.Text = euResolvoLogin;
                txtEuResolvoSenha.Text = DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoSenha", true);
                txtEuResolvoPeriodoRelatorio.Value = Convert.ToDecimal(DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoPeriodoRelatorio"));
                txtEuResolvoModoAutomatico.Checked = bool.Parse(DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoModoAutomatico"));

                //FTP
                txtFtpHost.Text = DadosDeConfiguracao.ObtenhaValor("cfgFtpHost");
                txtFtpPasta.Text = DadosDeConfiguracao.ObtenhaValor("cfgFtpPasta");
                txtFtpLogin.Text = DadosDeConfiguracao.ObtenhaValor("cfgFtpLogin", true);
                txtFtpSenha.Text = DadosDeConfiguracao.ObtenhaValor("cfgFtpSenha", true);
                txtFtpPorta.Text = DadosDeConfiguracao.ObtenhaValor("cfgFtpPorta");
                txtFtpAnonimo.Checked = bool.Parse(DadosDeConfiguracao.ObtenhaValor("cfgFtpAnonimo"));
            }
        }

        private void btnSalvar_Click(object sender, System.EventArgs e)
        {
            //Eu Resolvo
            DadosDeConfiguracao.SalveValor("cfgEuResolvoLogin", txtEuResolvoLogin.Text, true);
            DadosDeConfiguracao.SalveValor("cfgEuResolvoSenha", txtEuResolvoSenha.Text, true);
            DadosDeConfiguracao.SalveValor("cfgEuResolvoPeriodoRelatorio", txtEuResolvoPeriodoRelatorio.Text);
            DadosDeConfiguracao.SalveValor("cfgEuResolvoModoAutomatico", txtEuResolvoModoAutomatico.Checked.ToString());

            //FTP
            DadosDeConfiguracao.SalveValor("cfgFtpHost", txtFtpHost.Text);
            DadosDeConfiguracao.SalveValor("cfgFtpPasta", txtFtpPasta.Text);
            DadosDeConfiguracao.SalveValor("cfgFtpLogin", txtFtpLogin.Text, true);
            DadosDeConfiguracao.SalveValor("cfgFtpSenha", txtFtpSenha.Text, true);
            DadosDeConfiguracao.SalveValor("cfgFtpPorta", txtFtpPorta.Text);
            DadosDeConfiguracao.SalveValor("cfgFtpAnonimo", txtFtpAnonimo.Checked.ToString());

            var mensagem = new AlertaMensagem("Configuração salva com sucesso!", true);
            mensagem.ShowDialog();
            Close();
        }

        private void btnCancelar_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        //private void btnNovo_Click(object sender, System.EventArgs e)
        //{
        //    using (var frm = new FrmFornecedor())
        //    {
        //        frm.ShowDialog();

        //        if (frm.DialogResult == DialogResult.OK)
        //        {
        //            var sListaFornecedor = DadosDeConfiguracao.ObtenhaValor("ListaFornecedor");

        //            if (!string.IsNullOrEmpty(sListaFornecedor))
        //            {
        //                var listaDeFornecedores = sListaFornecedor.Split(';').Select(x => new { Fornecedor = x }).ToList();

        //                //gridFornecedor.DataSource = listaDeFornecedores;
        //                //gridFornecedor.Refresh();
        //            }
        //        }
        //    }
        //}

        //private void btnRemover_Click(object sender, System.EventArgs e)
        //{
        //    if (gridFornecedor.SelectedRows.Count > 0)
        //    {
        //        var selecionada = gridFornecedor.SelectedRows[0]?.Cells["Fornecedor"]?.Value.ToString();
        //        var sListaFornecedor = DadosDeConfiguracao.ObtenhaValor("ListaFornecedor");
        //        var listaDeFornecedores = sListaFornecedor.Split(';').Select(x => new { Fornecedor = x }).ToList();

        //        listaDeFornecedores.Remove(new { Fornecedor = selecionada });

        //        sListaFornecedor = string.Join(";", listaDeFornecedores.Select(x => x.Fornecedor).ToList());

        //        DadosDeConfiguracao.SalveValor("ListaFornecedor", sListaFornecedor);

        //        gridFornecedor.DataSource = listaDeFornecedores;
        //        gridFornecedor.Refresh();
        //    }
        //}

        private void txtEuResolvoSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.V || e.KeyCode == Keys.C))
            {
                e.SuppressKeyPress = true;
            }
        }

        private void txtSenhaSisjur_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.V || e.KeyCode == Keys.C))
            {
                e.SuppressKeyPress = true;
            }
        }

        private void txtEuResolvoLogin_Click(object sender, System.EventArgs e)
        {

        }

        private void txtSenhaConsumidorGov_Click(object sender, System.EventArgs e)
        {

        }

        private void txtUsuarioSisjur_Click(object sender, System.EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, System.EventArgs e)
        {

        }

        private void metroLabel2_Click(object sender, System.EventArgs e)
        {

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void lblPortalEuResolvoLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                IrParaPortalEuResolvo();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao abrir o link: " + ex);
            }
        }

        private void IrParaPortalEuResolvo()
        {
            lblPortalEuResolvoLink.LinkVisited = true;
            System.Diagnostics.Process.Start("https://oicolaborador-report.mobicare.com.br/sgi/login.htm");
        }

        private void txtPeriodoRelatorio_ValueChanged(object sender, EventArgs e)
        {

        }

        private void FrmConfiguracao_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEuResolvoModoAutomatico_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtFtpAnonimo_CheckedChanged(object sender, EventArgs e)
        {
            if (txtFtpAnonimo.Checked)
            {
                txtFtpLogin.BackColor = System.Drawing.Color.LightGray;
                txtFtpSenha.BackColor = System.Drawing.Color.LightGray;

                txtFtpLogin.Text = "anonymous";
                txtFtpSenha.Text = "";

                txtFtpLogin.ReadOnly = true;
                txtFtpSenha.ReadOnly = true;
            }
            else
            {
                txtFtpLogin.BackColor = System.Drawing.Color.White;
                txtFtpSenha.BackColor = System.Drawing.Color.White;

                txtFtpLogin.ReadOnly = false;
                txtFtpSenha.ReadOnly = false;
            }
        }
    }
}
