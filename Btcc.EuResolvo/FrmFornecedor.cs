﻿using Btcc.Infraestrutura.Util;
using MetroFramework.Forms;
using System.Windows.Forms;

namespace Btcc.ConsumidorGov
{
    public partial class FrmFornecedor : MetroForm
    {
        public FrmFornecedor()
        {
            InitializeComponent();
        }

        private void btnAdicionar_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFornecedor.Text))
            {
                var sListaFornecedor = DadosDeConfiguracao.ObtenhaValor("ListaFornecedor");
                var separador = string.IsNullOrEmpty(sListaFornecedor) ? string.Empty : ";";

                sListaFornecedor = string.Concat(sListaFornecedor, separador, txtFornecedor.Text);

                DadosDeConfiguracao.SalveValor("ListaFornecedor", sListaFornecedor);
            }

            DialogResult = DialogResult.OK;

            Close();
        }
    }
}
