﻿using Btcc.EuResolvo.Aplicacao.Servicos;
using Btcc.EuResolvo.Aplicacao.Web;
using Btcc.Infraestrutura.Dominio;
using Btcc.Infraestrutura.Interfaces;
using Btcc.Infraestrutura.Util;
using System;
using System.IO;
using System.Text;

namespace Btcc.EuResolvo
{
    public class PluginEuResolvo : PluginAutomatizacao, IPluginAutomatizacao
    {
        private readonly string _descricaoAutomatizacao = "Ouvidoria Eu Resolvo";

        public override string DescricaoDaAutomacao
        {
            get
            {
                return _descricaoAutomatizacao;
            }
        }

        public override Guid Identificador
        {
            get
            {
                return new Guid("985DF51B-FF0D-4B97-9146-2BCD0CE5EDB9");
            }
        }

        public override ResultadoAutomatizacao Configurar()
        {
            var configuracao = new FrmConfiguracao();
            configuracao.BringToFront();
            configuracao.ShowDialog();
            return base.Configurar();
        }

        public override ResultadoAutomatizacao Executar()
        {
            var resultado = new ResultadoAutomatizacao();

            var log = new ServicoEuResolvo();
            log.EscreveLog("Automatização iniciada");
            //new ServicoEuResolvo().InsereBD("Automatização iniciada");

            //Eu Resolvo
            var euResolvoLogin = DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoLogin", true);
            var euResolvoSenha = DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoSenha", true);
            int euResolvoPeriodoRelatorio = Convert.ToInt32(DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoPeriodoRelatorio"));
            bool euResolvoModoAutomatico = bool.Parse(DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoModoAutomatico"));
            var dataFim = DateTime.UtcNow.HorarioDeBrasilia();
            var dataInicio = dataFim.AddDays(euResolvoPeriodoRelatorio * -1);

            //FTP
            var ftpHost = DadosDeConfiguracao.ObtenhaValor("cfgFtpHost");
            var ftpPasta = DadosDeConfiguracao.ObtenhaValor("cfgFtpPasta");
            var ftpLogin = DadosDeConfiguracao.ObtenhaValor("cfgFtpLogin", true);
            var ftpSenha = DadosDeConfiguracao.ObtenhaValor("cfgFtpSenha", true);
            var ftpPorta = DadosDeConfiguracao.ObtenhaValor("cfgFtpPorta");
            //bool ftpAnonimo = bool.Parse(DadosDeConfiguracao.ObtenhaValor("cfgFtpAnonimo"));

            var pastaTemp = Path.GetTempPath();

            using (var portal = new PortalEuResolvo())
            {
                try
                {
                    if (string.IsNullOrEmpty(euResolvoLogin) || string.IsNullOrEmpty(euResolvoSenha))
                    {
                        log.EscreveLog("Erro: Login e/ou senha não foram configurados (vazios ou nulos)");
                        //new ServicoEuResolvo().InsereBD("Erro: Login e/ou senha não foram configurados (vazios ou nulos)");

                        throw new Exception();
                    }

                    if (portal.EfetueLogin(euResolvoLogin, euResolvoSenha))
                    {
                        log.EscreveLog("Login e senha OK");
                        //new ServicoEuResolvo().InsereBD("Login e senha OK");

                        // String abaixo utilizada para completar o link de download do relatório
                        // Exemplo: dateRangeFrom=23%2F07%2F2020&dateRangeTo=24%2F07%2F2020
                        var relatorio = portal.ObterRelatorioEuResolvo(pastaTemp, "dateRangeFrom="
                            + dataInicio.Day.ToString("d2") + "%2F"
                            + dataInicio.Month.ToString("d2") + "%2F"
                            + dataInicio.Year.ToString() + "&dateRangeTo="
                            + dataFim.Day.ToString("d2") + "%2F"
                            + dataFim.Month.ToString("d2") + "%2F"
                            + dataFim.Year.ToString()
                        );
                        log.EscreveLog(Path.GetFileName(relatorio).ToString() + " criado");
                        //new ServicoEuResolvo().InsereBD(Path.GetFileName(relatorio).ToString() + " criado");

                        // Adiciona uma nova linha no arquivo csv
                        GravaNovaLinhaEmArquivo(relatorio, "Nova linha a ser ignorada...");

                        // Upload para FTP
                        //portal.UploadRelatorioFtp(ftpHost, ftpPasta, ftpLogin, ftpSenha, ftpPorta, relatorio);

                        // Arquivo csv para lista de objetos
                        var listRelatorio = new ServicoEuResolvo().GeraListaRelatorio(relatorio);

                        log.EscreveLog(Path.GetFileName(relatorio) + " -> Lista de objetos OK");
                        //new ServicoEuResolvo().InsereBD(Path.GetFileName(relatorio).ToString() + " -> Lista de objetos OK");

                        // Input da lista de objetos para o Sisweb utilizando a API
                        log.EscreveLog("Input para o Sisweb via API iniciado");
                        //new ServicoEuResolvo().InsereBD("Input para o Sisweb via API iniciado");

                        new ServicoEuResolvo().EnviaParaApi(listRelatorio);

                        log.EscreveLog("Input finalizado");
                        //new ServicoEuResolvo().InsereBD("Input finalizado");

                        // Apagar relatório da pasta temporária local
                        File.Delete(relatorio);

                        log.EscreveLog(Path.GetFileName(relatorio) + " removido");
                        //new ServicoEuResolvo().InsereBD(Path.GetFileName(relatorio).ToString() + " removido");

                        log.EscreveLog("Automatização finalizada com sucesso");
                        //new ServicoEuResolvo().InsereBD("Automatização finalizada com sucesso");
                    }
                    else
                    {
                        log.EscreveLog("Erro: Login e/ou senha incorretos");
                        //new ServicoEuResolvo().InsereBD("Erro: Login e/ou senha incorretos");
                        throw new Exception();
                    }

                    //return new ResultadoAutomatizacao();
                }
                catch (Exception ex)
                {
                    resultado.Sucesso = false;
                    resultado.Erro = ex;

                    log.EscreveLog("Erro: " + ex);
                    log.EscreveLog("Automatização finalizada com erros");
                    //new ServicoEuResolvo().InsereBD("Erro: " + ex);
                    //new ServicoEuResolvo().InsereBD("Automatização finalizada com erros");

                    //return new ResultadoAutomatizacao();
                }
                finally
                {
                    log.EscreveLog("", false);
                    //if (euResolvoModoAutomatico)
                    //{
                    //    Environment.Exit(1);
                    //}
                }

                return resultado;
            }
        }

        public override ParametrosPluginAutomatizacao ObtenhaParametrosDeConfiguracao()
        {
            return new ParametrosPluginAutomatizacao
            {
                //ApresentarBotaoAgendar = false,
                ApresentarBotaoConfigurar = true,
                //ApresentarLabelUltimaExecucao = false,
                DescricaoAutomatizacao = _descricaoAutomatizacao
            };
        }

        private static void GravaNovaLinhaEmArquivo(string caminho, string novaLinha)
        {
            string conteudo = File.ReadAllText(caminho);
            conteudo = novaLinha + "\n" + conteudo;
            File.WriteAllText(caminho, conteudo, Encoding.UTF8);
        }
    }
}