﻿using Btcc.Infraestrutura.Util;
using MetroFramework.Forms;
using System;

namespace Btcc.EuResolvo
{
    public partial class FrmPrincipal : MetroForm
    {
        private PluginEuResolvo _plugin;

        public FrmPrincipal()
        {
            InitializeComponent();
            _plugin = new PluginEuResolvo();
            bool execucaoAutomatica = bool.Parse(DadosDeConfiguracao.ObtenhaValor("cfgEuResolvoModoAutomatico"));

            if (execucaoAutomatica)
                _plugin.Executar();
        }

        private void btnExecutar_Click(object sender, EventArgs e)
        {
            _plugin.Executar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _plugin.Configurar();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
