﻿using Btcc.EuResolvo.Aplicacao.Csv;
using Btcc.EuResolvo.Aplicacao.Entidades;
using Btcc.Infraestrutura.ManipulacaoDeArquivos;
using Btcc.Infraestrutura.Util;
using Btcc.Infraestrutura.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;

namespace Btcc.EuResolvo.Aplicacao.Servicos
{
    public class ServicoEuResolvo : IDisposable
    {
        public void Dispose()
        {
        }

        public void InsereBD(string mensagem)
        {
            OracleConnection conn = new OracleConnection();

            // Homologação
            conn.ConnectionString = "Password=gamouse;Persist Security Info=True;User ID=UNIFOUVID;Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.172.1.94)(PORT=1521))(CONNECT_DATA = (SERVICE_NAME = DEVSISWEB)));Pooling=true; Min Pool Size=1;Max Pool Size=50;Enlist=False;";

            // Produção

            conn.Open();

            var cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO LOG_EURESOLVO(LOEU_ID, LOEU_DATA, LOEU_DESCRICAO) VALUES (LOG_EURESOLVO_SEQ.nextval, LOCALTIMESTAMP, :pMENSAGEM)";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(":pMENSAGEM", OracleDbType.Varchar2, mensagem, ParameterDirection.Input);

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public List<EuResolvoEnt> GeraListaRelatorio(string arquivoFonte)
        {
            if (!string.IsNullOrEmpty(arquivoFonte))
            {
                var listaRelatorio = ConversorDeArquivos.ConvertaArquivo<EuResolvoEnt, EuResolvoMap>(arquivoFonte);
                List<EuResolvoEnt> listaRelatorioFiltrada = new List<EuResolvoEnt>();
                int protocolosASeremInseridos = 0;
                int protocolosTotal = 1;

                foreach (var item in listaRelatorio)
                {
                    if (item.StatusEuResolvo == "Em aberto")
                    {
                        if (item.CpfCnpjCliente != null && item.CpfCnpjCliente != "null")
                        {
                            if (item.CpfCnpjCliente.Length < 14)
                            {
                                // CPF
                                item.CpfCnpjCliente = Convert.ToUInt64(item.CpfCnpjCliente.ToString()).ToString(@"000\.000\.000\-00");
                            }
                            else
                            {   // CNPJ
                                item.CpfCnpjCliente = Convert.ToUInt64(item.CpfCnpjCliente.ToString()).ToString(@"00\.000\.000/0000\-00");
                            }
                        }

                        listaRelatorioFiltrada.Add(item);
                        protocolosASeremInseridos++;
                    }

                    protocolosTotal++;
                }

                foreach (var item in listaRelatorioFiltrada)
                {
                    item.Id = 0;
                    item.Mailing = "OUVIDORIA - Eu Resolvo";
                    item.Status = "NAO_INICIADO";
                    item.DataDeVencimento = default(DateTime);

                    if (string.Equals(item.TipoCliente, "Empresarial", StringComparison.OrdinalIgnoreCase))
                    {
                        item.NomeDaFila = "OUV. - EU RESOLVO EMP";
                    }
                    else
                    {
                        item.NomeDaFila = "OUV. - EU RESOLVO PF";
                    }
                }

                //int protocolosConcluidos = protocolosTotal - protocolosASeremInseridos;
                EscreveLog("Total geral de protocolos encontrados no arquivo csv: " + protocolosTotal);
                EscreveLog("Total de protocolos com o status 'Em aberto': " + protocolosASeremInseridos);
                //EscreveLog("Protocolos disponíveis para inserção: " + protocolosASeremInseridos);

                return listaRelatorioFiltrada.ToList();
            }

            return new List<EuResolvoEnt>();
        }

        public void EnviaParaApi(IList<EuResolvoEnt> lista)
        {
            if (!lista.Any())
                return;

            var UrlSisweb = DadosDeConfiguracao.ObtenhaValor("UrlSisweb");
            var listaDividida = DividaListaDeObjetos(lista.ToList(), 50);

            foreach (var subLista in listaDividida)
            {
                // Melhoria solicitada pelo Dilson, relacionada ao projeto do Unificado Ouvidoria (2020-09-03)
                EnriquecaComResponsavel(subLista);

                var enumerable = subLista.ToArray();
                var dados = new
                {
                    json = enumerable
                };

                var dadosJson = JsonConvert.SerializeObject(dados);
                var url = $"{UrlSisweb}Protocolo";
                var resultado = ProcessamentosWebApi.ExecutaRequisicao(url, dadosJson, TipoOperacao.Post);

                if (!resultado.Key.Equals(HttpStatusCode.OK))
                {
                    EscreveLog("Erro ao cadastrar protocolos");
                    EscreveLog("    --> HTTP Status Code: " + resultado.Key);
                    EscreveLog("    --> Url: " + url);
                    EscreveLog("    --> Descrição: " + resultado.Value);
                    // EscreveLog("", false);
                }else {
                    JObject array = JObject.Parse(resultado.Value);
                    List<EuResolvoEnt> listaProtocolos = new List<EuResolvoEnt>();
                    foreach (var objeto in array["json"])
                    {
                        var protocolo = JsonConvert.DeserializeObject<EuResolvoEnt>(objeto.ToString());
                        protocolo.ProtocoloSac = subLista.FirstOrDefault(r => r.IdEuResolvo.Equals(protocolo.IdEuResolvo))?.ProtocoloSac;
                        listaProtocolos.Add(protocolo);
                    }

                    InseriProtocoloSac(listaProtocolos);
                }
            }
        }

        // Melhoria solicitada pelo Dilson, relacionada ao projeto do Unificado Ouvidoria (2020-09-03)
        private void EnriquecaComResponsavel(IList<EuResolvoEnt> registros)
        {
            var UrlSisweb = DadosDeConfiguracao.ObtenhaValor("UrlSisweb");

            var restricoes = new List<object>()
             {
                new { Coluna = "C04", Valor = registros
                    .Where(r => (r.CpfCnpjCliente ?? string.Empty) != string.Empty)
                    .Where(r => (r.CpfCnpjCliente != "null"))
                    .Where(r => (r.CpfCnpjCliente != null))
                    .Select(r => r.CpfCnpjCliente)}
             };

            var mailing = "OUVIDORIA - Eu Resolvo";
            var json = JsonConvert.SerializeObject(restricoes);
            var resultado = ProcessamentosWebApi.ExecutaRequisicao($"{UrlSisweb}Protocolo/ConsultePorPerfilamentoMesCorrente?nomeDoMailing={mailing}", json, TipoOperacao.Post);
            var novaLista = new List<EuResolvoEnt>();

            if (resultado.Key.Equals(HttpStatusCode.OK))
            {
                novaLista = JsonConvert.DeserializeObject<IList<EuResolvoEnt>>(resultado.Value).ToList();
            }

            foreach (var registro in registros)
            {
                registro.Responsavel = novaLista.FirstOrDefault(r => r.CpfCnpjCliente.Equals(registro.CpfCnpjCliente))?.Responsavel;
            }
        }

        private void InseriProtocoloSac(IList<EuResolvoEnt> registros)
        {
            var UrlSisweb = DadosDeConfiguracao.ObtenhaValor("UrlSisweb");
            foreach (var protocolo in registros)
            {
                var alteraColuna = new List<object>()
                {
                    new { Coluna = "C20", Valor = protocolo.ProtocoloSac }
                };
                
                var json = JsonConvert.SerializeObject(alteraColuna);
                var resultado = ProcessamentosWebApi.ExecutaRequisicao($"{UrlSisweb}Protocolo/AtualizaCamposProtocolos?idProtocolo={protocolo.Id}", json, TipoOperacao.Post);
            }
        }

        public void EscreveLog(string textoLog, bool inserirDataLog = true)
        {
            var caminhoDiretorio = AppDomain.CurrentDomain.BaseDirectory + "\\EuResolvoLogs";

            if (!Directory.Exists(caminhoDiretorio))
            {
                Directory.CreateDirectory(caminhoDiretorio);
            }

            string caminhoArquivo = caminhoDiretorio + "\\EuResolvoLog_" + DateTime.Today.ToString("yyyy_MM_dd") + ".txt";

            if (!File.Exists(caminhoArquivo))
            {
                File.Create(caminhoArquivo).Close();
            }

            string dataLog = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - ";
            TextWriter arquivo = File.AppendText(caminhoArquivo);

            if (inserirDataLog)
            {
                arquivo.WriteLine(dataLog + textoLog);
            }
            else
            {
                arquivo.WriteLine(textoLog);
            }
            arquivo.Close();
        }

        private IList<List<TType>> DividaListaDeObjetos<TType>(List<TType> listaObjetos, int tamanho)
        {
            var list = new List<List<TType>>();

            if (listaObjetos == null)
            {
                return list;
            }

            for (int i = 0; i < listaObjetos.Count; i += tamanho)
            {
                list.Add(listaObjetos.GetRange(i, Math.Min(tamanho, listaObjetos.Count - i)));
            }

            return list;
        }
    }
}