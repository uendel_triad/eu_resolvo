﻿$.fn.trimPropriedades = function (objeto) {
    $.each(objeto, function (propriedade, valor) {
        if (typeof (valor) == "string") {
            objeto[propriedade] = $.trim(valor);
        } else if (typeof (objeto[propriedade]) == "object") {
            $.fn.trimPropriedades(objeto[propriedade]);
        }
    });
}

/* Função que define o valor de uma propriedade a partir de uma string separada por ".". */
/* Definido no objeto window para ser invocado pela função definaNamespace. */
var setaValor = (function () {

    var setaValorString = function (objetoPai, caminho, valor) {
        var arrayCaminho = caminho.split('.');
        setaValorArray(objetoPai, arrayCaminho, valor);
    };

    var setaValorArray = function funcaoSetaValorArray(objetoPai, caminho, valor) {
        var nome = caminho[0];
        if (caminho.length > 1) {
            if (!objetoPai[nome]) {
                objetoPai[nome] = {};
            }

            funcaoSetaValorArray(objetoPai[nome], caminho.slice(1, caminho.length), valor);
        } else {
            definaValor(objetoPai, nome, valor);
        }
    };

    var definaValor = function (objetoPai, nomePropriedade, valor) {
        var valorAtual = objetoPai[nomePropriedade];
        if (valorAtual) {
            objetoPai[nomePropriedade] = jQuery.extend(valorAtual, valor);
        } else {
            objetoPai[nomePropriedade] = valor;
        }
    };

    return setaValorString;
})();

/* Define um namespace btcc. */
var definaNamespace = function (namespace, conteudo) {
    if (namespace.substr(0, 4) != "btcc") {
        namespace = "btcc." + namespace;
    }

    setaValor(window, namespace, conteudo);
};

(function ($) {
    jQuery.fn.convertaParaJson = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (this.value == "true" || o[this.name] == "true") {  /* tratando input hidden gerado para checkbox. */
                    o[this.name] = "true";
                } else {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                }
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

})(jQuery);

definaNamespace('btcc.util.json');
btcc.util.json = (function () {
    var convertaDadosDeFormularioEmObjeto = function (dados, objetoDestino) {
        for (var propriedade in dados) {
            if (typeof propriedade == "string") {
                try {
                    var resultDoParse = JSON.parse(dados[propriedade], function (k, valor) {
                        /* 
                        Este trecho de código apresenta erro no firefox 22, que não remove do DOM formulários acessados.
                        Então, este trecho sempre retornava o valor do código editado pela primeira vez.
                        Provavelmente obsoleto.
                        if (window[propriedade] && window[propriedade].value) {
                        return window[propriedade].value;
                        }
                        */
                        return valor;
                    });

                    if (typeof resultDoParse == "object") {
                        dados[propriedade] = crieListaObjetosPriedadesComplexasDeListaObjetosPropriedadesPlanas(resultDoParse);
                    }
                } catch (err) {

                }
            }
        }
        copieDadosObjetoPlanoParaObjetoComplexo(dados, objetoDestino);
    };

    var crieListaObjetosPriedadesComplexasDeListaObjetosPropriedadesPlanas = function (propriedade) {
        if (obtenhaTipo(propriedade) == "Array") {
            var novaLista = new Array();
            for (var j = 0; j < propriedade.length; j++) {
                var objetoOrigem = propriedade[j];
                if (obtenhaTipo(objetoOrigem) == "Object") {
                    var objetoDestino = {};
                    copieDadosObjetoPlanoParaObjetoComplexo(objetoOrigem, objetoDestino);
                    novaLista.push(objetoDestino);
                } else {
                    novaLista.push(propriedade[j]);
                }
            }

            return novaLista;
        } else {
            return propriedade;
        }
    };

    var copieDadosObjetoPlanoParaObjetoComplexo = function (objetoOrigem, objetoDestino) {

        for (var propriedadeComplexa in objetoOrigem) {
            var listaPropriedadesSimples = propriedadeComplexa.split('.');
            var objetoPropriedadeAtual = objetoDestino;
            for (var i = 0; i < listaPropriedadesSimples.length - 1; i++) {
                var propriedade = listaPropriedadesSimples[i]
                var propriedadeEhVetorCustomizado = propriedade.indexOf("[") != -1;
                if (propriedadeEhVetorCustomizado) {
                    var posicaoElemento = parseInt(propriedade.substring(propriedade.indexOf('[') + 1, propriedade.length - 1));
                    propriedade = propriedade.replace(/\[[0-9]+\]/g, '');

                    if (objetoPropriedadeAtual[propriedade] == null) {
                        objetoPropriedadeAtual[propriedade] = new Array();
                    }

                    var objeto = objetoPropriedadeAtual[propriedade][posicaoElemento] || {};
                    objetoPropriedadeAtual[propriedade][posicaoElemento] = objeto;
                    objetoPropriedadeAtual = objeto;
                } else {
                    if (objetoPropriedadeAtual[propriedade] == null) {
                        objetoPropriedadeAtual[propriedade] = {};
                    }

                    objetoPropriedadeAtual = objetoPropriedadeAtual[propriedade];
                }
            }

            if (listaPropriedadesSimples.length - 1 >= 0) {
                var propriedade = listaPropriedadesSimples[listaPropriedadesSimples.length - 1];
                var propriedadeEhVetorCustomizado = propriedade.indexOf("[") != -1;
                var objeto = objetoOrigem[propriedadeComplexa];
                if (propriedadeEhVetorCustomizado) {
                    var posicaoElemento = parseInt(propriedade.substring(propriedade.indexOf('[') + 1, propriedade.length - 1));
                    propriedade = propriedade.replace(/\[[0-9]+\]/g, '');

                    if (objetoPropriedadeAtual[propriedade] == null) {
                        objetoPropriedadeAtual[propriedade] = new Array();
                    }

                    objetoPropriedadeAtual[propriedade][posicaoElemento] = objeto;
                } else {
                    objetoPropriedadeAtual[propriedade] = objeto;
                }
            }
        }
    };

    var convertaParaJsonComHidden = function (campos) {
        var myform = $('body');
        var desabilitados = myform.find(':input:disabled, :selected:disabled').prop('disabled', false).removeAttr('disabled');
        var dados = $(campos).convertaParaJson();
        desabilitados.attr('disabled', 'disabled').prop('disabled', true);
        $.fn.trimPropriedades(dados);
        return dados;
    };

    /* Serializa os dados do formulário a partir do seletor informado. */
    var serializeDados = function (seletor) {
        var dados = convertaParaJsonComHidden(seletor);
        var objeto = {};
        btcc.util.json.convertaDadosDeFormularioEmObjeto(dados, objeto);
        return objeto;
    };

    return {
        convertaDadosDeFormularioEmObjeto: convertaDadosDeFormularioEmObjeto,
        serializeDados: serializeDados
    };
})(btcc.util.json || undefined);

var crieModal = function (id, labelTitulo, conteudo, listaDeBotoes, largura) {
    var _this = this;

    var stringHtmlModal = "" +
        "<div id='" + id + "' class='modal' role='dialog' data-backdrop='static' data-keyboard='false'>" +
            "<div class='modal-dialog' role='document' style='width: 97%; max-width:" + (largura != null ? largura : "750") + "px;'>" +
                "<div class='modal-content'>" +
                    "<div class='modal-header'>" +
                        "<h4 class='modal-title '>" + labelTitulo + "</h4>" +
                    "</div>" +
                    "<div class='modal-body'>" +
                        conteudo +
                    "</div>" +
                    "<div class='modal-footer botoesModal'>" +
                    "</div>" +
                "</div>" +
            "</div>" +
        "</div>";

    var $modal = $(stringHtmlModal);

    var $elBotoesModal = $modal.find('div.botoesModal');

    for (var i = 0; i < listaDeBotoes.length; i++) {
        var possuiDataDismiss = $elBotoesModal.find('button[data-dismiss]').length > 0;
        var ehUltimo = i == listaDeBotoes.length - 1;
        var objBotao = listaDeBotoes[i];

        var $elBotao = $("<button type='" + ('type' in objBotao ? objBotao.type : 'button') + "' class='btn " + objBotao.classes + "'>" + objBotao.label + "</button>");

        if ((objBotao.ehCancelar || ehUltimo) && !possuiDataDismiss) {
            $elBotao.attr('data-dismiss', 'modal');
        }

        if (objBotao.evento) {
            $elBotao.on('click', objBotao.evento);
        }

        $elBotao.appendTo($elBotoesModal);
    }

    return $modal;
};

var obtenhaPrimeiroIndice = function (nomePropriedade) {
    if (nomePropriedade == null || nomePropriedade == '') {
        return null;
    }

    var lista = nomePropriedade.match("\\[\\d+\\]");
    if (lista == null || lista.length == 0) {
        return null;
    }

    var indices = [];
    $(lista).each(function (indice, item) {
        indices.push(parseInt(item.replace('[', '').replace(']', ''), 10));
    });

    return indices.length > 0 ? indices[0] : null;
};

var modalPossuiErros = function ($modal) {
    return $modal.find('.has-error .with-errors:visible').length > 0;
};

var ehResultadoHtml = function (resultado) {
    var objResultado = (function (str) {
        try {
            return typeof (str) == "object" || JSON.parse(str);
        } catch (err) {
            return false;
        }
    })(resultado);

    return !objResultado;
};

var exibaInconsistencia = function (mensagem) {
    var dadosValidacao = '<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>' + mensagem + '</div>';
    var elemento = $('.containerMensagensDeExcecao').last();
    elemento.append(dadosValidacao);
    elemento[0].scrollIntoView();
};

var trateExcecaoPadrao = function (resultado) {
    var mensagem = '';
    if (resultado.stack) {
        mensagem = resultado.message;
    } else if (resultado.Sucesso != null && !resultado.Sucesso) {
        mensagem = resultado.Mensagem;
        if (resultado.Excecao) {
            console.log(resultado.originalEvent.error.Excecao);
        }
    } else if (resultado.originalEvent != null && resultado.originalEvent.error != null) {
        if (resultado.originalEvent.error.Mensagem != null) {
            mensagem = resultado.originalEvent.error.Mensagem;
            if (resultado.originalEvent.error.Excecao) {
                console.log(resultado.originalEvent.error.Excecao);
            }
        } else if (resultado.originalEvent.error.message != null) {
            mensagem = resultado.originalEvent.error.message;
        } else if (typeof (resultado.originalEvent.error) == 'string') {
            mensagem = resultado.originalEvent.error;
        } else if (resultado.originalEvent.error.Validacoes != null) {
            trateValidacoesDeNegocio(resultado.originalEvent.error.Validacoes);
        }
    } else {
        try {
            var dadosExcecao = resultado.responseJSON != null ? resultado.responseJSON : JSON.parse(resultado.responseText);
            console.log(dadosExcecao);
            if (dadosExcecao != null
                && dadosExcecao.Sucesso != null
                && !dadosExcecao.Sucesso) {
                mensagem = dadosExcecao.Mensagem;
            }
        } catch (ex) {
            console.log(ex);
            mensagem = "Erro ao executar a ação desejada.";
        }
    }

    if (mensagem != '') {
        exibaInconsistencia(mensagem);
    }
};

var trateValidacoesDeNegocio = function (validacoes) {
    $('li.with-errors').removeClass('with-errors');

    var mensagensSemCampo = [];
    for (var i = 0; i < validacoes.length; i++) {
        var propriedade = validacoes[i].PropertyName;
        var mensagem = validacoes[i].ErrorMessage;

        var campo = $('[name="' + propriedade + '"]');
        if (!campo.length) {
            campo = $('[id="' + propriedade + '"]');
        }

        if (!campo.length) {
            mensagensSemCampo.push(mensagem);
        } else {
            campo.parents('.form-group').addClass('has-error').addClass('has-danger');
            var dadosMensagem = campo.siblings('.help-block');
            dadosMensagem.html('');
            dadosMensagem.append('<ul class="list-unstyled"><li>' + mensagem + '</li></ul>');

            if (campo.parents('.tab-pane')) {
                var idAba = campo.parents('.tab-pane').attr('id');
                var aba = $('a[href="#' + idAba + '"]');
                if (aba) {
                    aba.parent().addClass('with-errors');
                }
            }
        }
    }

    exibaInconsistencia('Atenção! Algumas inconsistências foram encontradas.');
    if (mensagensSemCampo.length > 0) {
        for (var i = 0; i < mensagensSemCampo.length; i++) {
            exibaInconsistencia(mensagensSemCampo);
        }
    }
};

$(window).ready(function () {
    $('.loadingContainer').hide();
});

window.loadingEvent = null;
$(document).ajaxStart(function () {
    window.loadingEvent = setTimeout(function () {
        $('.loadingContainer').show();
    }, 500);
});

$(document).ajaxStop(function () {
    clearTimeout(window.loadingEvent);
    $('.loadingContainer').hide();
});

$(window).on('error', function (e) {
    trateExcecaoPadrao(e);
    clearTimeout(window.loadingEvent);
    $('.loadingContainer').hide();
});

var trateExibicao = function ($elemento, exibir) {
    if (exibir) {
        $elemento.show();
    } else {
        $elemento.hide();
    }
};

var verifiqueExcecaoAjax = function (resultado) {
    try {
        if (typeof (resultado) == "string") {
            resultado = JSON.parse(resultado);
        }

        if (typeof (resultado) == "object" && resultado.Sucesso != null && !resultado.Sucesso) {
            throw resultado;
        }
    } catch (ex) {
        if (ex.Sucesso != null) {
            throw ex;
        }

        return;
    }
};

var ordenePorPropriedade = function (lista, propriedade) {
    return lista.sort(function (a, b) {
        var textA = a[propriedade].toUpperCase();
        var textB = b[propriedade].toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
};

function newGuid() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}