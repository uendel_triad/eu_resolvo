﻿definaNamespace('btcc.dropdownlist');

btcc.dropdownlist = function ($el, opcoes) {
    "use strict";
    if (!$el)
        throw "O Item não foi definido.";

    this.$el = $el;
    this.el = $el[0];
    this.opcoes = opcoes != null ? opcoes : {};

    this.$elCampo = this.$el.find('select'),

    this.$el.data("btcc.dropdownlist", this),

    this._inicialize();
};

btcc.dropdownlist.prototype = {
    _inicialize: function () {
        this._preparaComponente();
        this._ligaEventos();
    },

    _preparaComponente: function () {
        var _this = this;

        if (_this.opcoes.AutoComplete) {
            var objeto = {
                allowClear: true,
                // Para uso de HTML essa marcação deve ser adicionada
                escapeMarkup: function (markup) { return markup; },
                theme: "bootstrap",
                language: "pt-BR",
                placeholder: _this.opcoes.PlaceHolder != null ? _this.opcoes.PlaceHolder : '',
                tokenSeparators: [',', ';']
            };

            if (_this.opcoes.PermitirTags) {
                objeto.tags = true;
                objeto.createTag = function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        newTag: true // add additional parameters
                    }
                }
            }

            if (_this.opcoes.Url != null
                && _this.opcoes.Url != '') {
                objeto.ajax = {
                    url: _this.opcoes.Url,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filtro: params.term,
                            pagina: params.page,
                            opcoes: JSON.stringify(_this.opcoesDeBusca())
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data
                        };
                    }
                };
            }

            _this.$elCampo.select2(objeto);

            if (_this.opcoes.ValorInicial != null && _this.opcoes.ValorInicial != '') {
                var valorInicial = _this.opcoes.ValorInicial;

                if (valorInicial.indexOf(';') != -1) {
                    var valoresIniciais = valorInicial.split(';');
                    valorInicial = valoresIniciais;
                    _this.$elCampo.val(valorInicial);
                    _this.$elCampo.trigger('change');
                } else if (_this.$elCampo.find('option[value="' + valorInicial + '"]').length) {
                    _this.$elCampo.val(valorInicial);
                    _this.$elCampo.trigger('change');
                }

                if (_this.opcoes.PermitirTags) {
                    var data = {
                        id: valorInicial,
                        text: valorInicial
                    };

                    var newOption = new Option(data.text, data.id, false, true);
                    _this.$elCampo.append(newOption).trigger('change');
                }
            } else {
                _this.$elCampo.val('').change();
            }
        }
    },

    _ligaEventos: function () {
        var _this = this;

    },

    opcoesDeBusca: function () {
        return {}
    }
};