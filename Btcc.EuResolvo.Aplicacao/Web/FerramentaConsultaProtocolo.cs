﻿using Btcc.EuResolvo.Dominio.Objetos;
using Btcc.EuResolvo.Dominio.PageObjects;
using Btcc.Infraestrutura.AOP;
using Btcc.Infraestrutura.PageObjects;
using Btcc.Infraestrutura.Util;
using Btcc.Infraestrutura.WebDrivers;
using System.Collections.Generic;
using System.Linq;

namespace Btcc.EuResolvo.Aplicacao.Web
{
    public class FerramentaConsultaProtocolo : ChromeWebDriverBase
    {
        private string _login { get; set; }

        private string _senha { get; set; }

        private List<string> _listaDeFornecedor { get; set; }

        private FerramentaLoginPage _paginaDeLogin { get; set; }

        private FerramentaConsultaDetalhesProtocoloPage _paginaConsultaDetalhes { get; set; }

        public FerramentaConsultaProtocolo()
        {
            _login = DadosDeConfiguracao.ObtenhaValor("EuResolvoLogin", true);
            _senha = DadosDeConfiguracao.ObtenhaValor("EuResolvoSenha", true);

            var sListaFornecedor = DadosDeConfiguracao.ObtenhaValor("ListaFornecedor");
            _listaDeFornecedor = !string.IsNullOrEmpty(sListaFornecedor) ? sListaFornecedor.Split(';').ToList() : new List<string>();

            InicializeONavegador(false);

            _paginaDeLogin = FabricaAOP.GereInstancia<FerramentaLoginPage>(driver, new ChecagemDeTelaWeb(driver));
        }

        public List<Protocolo> ConsulteDadosDosProtocolos()
        {
            if (_paginaConsultaDetalhes == null || !_paginaDeLogin.UsuarioEstaLogado())
            {
                _paginaConsultaDetalhes = _paginaDeLogin.EfetueLogin(_login, _senha);
            }

            var listaDeDadosDosProtocolos = new List<Protocolo>();

            foreach (var fornecedor in _listaDeFornecedor)
            {
                _paginaConsultaDetalhes.SelecioneItemEmComboPorTexto(Constantes.ByComponentes.ComboFornecedorGerenciar, fornecedor, 2);
                _paginaConsultaDetalhes.SelecioneQuantidadeDeRegistrosPorPagina();
                _paginaConsultaDetalhes.PesquisePorDataAtualMenosUm();

                var protocolosGrid = _paginaConsultaDetalhes.ConsulteListaDeNumerosDeProtocolos();

                listaDeDadosDosProtocolos.AddRange(_paginaConsultaDetalhes.ConsulteDadosDosProtocolos(fornecedor, protocolosGrid));

                //_paginaConsultaDetalhes.CliqueEmElementoPorLinkText(protocolo, 2);
                //var elementos = _paginaConsultaDetalhes.ObtenhaInformacoesUsuario();
            }

            return listaDeDadosDosProtocolos;
        }
    }
}
