﻿using System;
using System.Net;

namespace Btcc.EuResolvo.Aplicacao.Web
{
    public class HttpHandler : WebClient
    {
        private CookieContainer _cookies = new CookieContainer();

        public bool AllowAutoRedirect { get; set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);

            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = _cookies;
                (request as HttpWebRequest).AllowAutoRedirect = AllowAutoRedirect;
            }

            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            var response = base.GetWebResponse(request);

            if (response is HttpWebResponse)
                _cookies.Add((response as HttpWebResponse).Cookies);

            return response;
        }

        public void ClearCookies()
        {
            _cookies = new CookieContainer();
        }

        protected override void Dispose(bool disposing)
        {
            ClearCookies();

            base.Dispose(disposing);
        }
    }
}
