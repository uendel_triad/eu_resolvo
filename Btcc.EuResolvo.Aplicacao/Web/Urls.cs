﻿namespace Btcc.EuResolvo.Aplicacao.Web
{
    public static class Urls
    {
        public const string Login = "https://oicolaborador-report.mobicare.com.br/sgi/j_spring_security_check";
        public const string Logoff = "https://oicolaborador-report.mobicare.com.br/sgi/j_spring_security_logout";
        public const string RelatorioEuResolvo = "https://oicolaborador-report.mobicare.com.br/sgi/secure/reports/export/csv/455.htm?";
        // Parâmetros adicionais para a string RelatorioEuResolvo acima
        // estão sendo utilizados no arquivo PluginEuResolvo.cs
        // dateRangeFrom=23%2F07%2F2020&dateRangeTo=24%2F07%2F2020
    }
}