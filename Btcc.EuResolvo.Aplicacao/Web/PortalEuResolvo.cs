﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace Btcc.EuResolvo.Aplicacao.Web
{
    public class PortalEuResolvo : IDisposable
    {
        private HttpHandler _clienteWeb;

        public PortalEuResolvo()
        {
            _clienteWeb = new HttpHandler();

            _clienteWeb.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
            _clienteWeb.Encoding = Encoding.UTF8;
            _clienteWeb.AllowAutoRedirect = true;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        public void Dispose()
        {
            if (_clienteWeb != null)
                _clienteWeb.Dispose();
        }

        public bool EfetueLogin(string usuario, string senha)
        {
            var postData = new NameValueCollection();

            postData.Add("j_username", usuario);
            postData.Add("j_password", senha);

            var result = _clienteWeb.UploadValues(new Uri(Urls.Login), postData);
            var resultado = UnicodeEncoding.UTF8.GetString(result);

            return !resultado.Contains("Usuário ou Senha incorreta");
        }

        public void EfetueLogoff()
        {
            _clienteWeb.UploadValues(new Uri(Urls.Logoff), new NameValueCollection());
        }

        public string ObterRelatorioEuResolvo(string pastaTemporaria, string periodoData)
        {
            var postData = new NameValueCollection();
            var uri = new Uri(Urls.RelatorioEuResolvo);
            //var desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            string nomeDoArquivo = pastaTemporaria + "EuResolvo_"
                + DateTime.Now.Year
                + DateTime.Now.Month.ToString("d2")
                + DateTime.Now.Day.ToString("d2")
                + "_"
                + DateTime.Now.Hour.ToString("d2")
                + DateTime.Now.Minute.ToString("d2")
                + DateTime.Now.Second.ToString("d2")
                + ".csv";

            _clienteWeb.DownloadFile(uri + periodoData, nomeDoArquivo);

            return nomeDoArquivo;
        }

        public void UploadRelatorioFtp(string host, string pasta, string login, string senha, string porta, string caminhoDoArquivo)
        {
            FtpWebRequest request;
            string nomeDoArquivo = Path.GetFileName(caminhoDoArquivo);

            request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}:{1}/{2}/{3}", host, porta, pasta, nomeDoArquivo))) as FtpWebRequest;
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UseBinary = true;
            request.UsePassive = true;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential(login, senha);
            request.ConnectionGroupName = "group";

            using (FileStream fs = File.OpenRead(caminhoDoArquivo))
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
            }
        }
    }
}