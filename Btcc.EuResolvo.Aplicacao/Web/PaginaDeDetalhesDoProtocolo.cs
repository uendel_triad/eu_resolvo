﻿using Btcc.EuResolvo.Dominio.Constantes;
using Btcc.EuResolvo.Dominio.Objetos;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Btcc.EuResolvo.Aplicacao.Web
{
    public class PaginaDeDetalhesDoProtocolo
    {
        private HttpHandler _clientWeb;
        private HtmlDocument _htmlDoc;

        public PaginaDeDetalhesDoProtocolo(HttpHandler clientWeb)
        {
            _clientWeb = clientWeb;
        }

        public PaginaDeDetalhesDoProtocolo CarregaPagina(string html)
        {
            if (!string.IsNullOrEmpty(html))
            {
                _htmlDoc = new HtmlDocument();
                _htmlDoc.LoadHtml(html);
            }
            else
            {
                _htmlDoc = null;
            }

            return this;
        }

        //public Protocolo ObtenhaProtocolo()
        //{
        //    if (_htmlDoc != null)
        //    {
        //        var protocolo = new Protocolo();

        //        PreenchaDadosPessoais(protocolo);
        //        PreenchaDadosProtocolo(protocolo);
        //        PreenchaInformacoesAdicionais(protocolo);
        //        PreenchaListaDeAnotacoes(protocolo);

        //        return protocolo;
        //    }

        //    return null;
        //}

        //private Detalhe ConsulteDetalhe(string tramite, string protocolo)
        //{
        //    if (!string.IsNullOrEmpty(tramite) && !string.IsNullOrEmpty(protocolo))
        //    {
        //        var uri = new Uri(string.Format(Urls.ConsultaDetalheAnotacao, tramite, protocolo));

        //        _clientWeb.Encoding = System.Text.Encoding.Default;

        //        var result = _clientWeb.DownloadString(uri);

        //        _clientWeb.Encoding = System.Text.Encoding.UTF8;

        //        if (!string.IsNullOrEmpty(result))
        //        {
        //            return JsonConvert.DeserializeObject<Detalhe>(result);
        //        }
        //    }

        //    return null;
        //}

        //private List<Anexo> DownloadListaDeAnexos(string tramite, string protocolo)
        //{
        //    var listaDeAnexo = new List<Anexo>();

        //    if (!string.IsNullOrEmpty(tramite) && !string.IsNullOrEmpty(protocolo))
        //    {
        //        var uri = new Uri(string.Format(Urls.ConsultaListaDeAnexos, tramite, protocolo));

        //        var result = _clientWeb.UploadString(uri, string.Empty);

        //        if (!string.IsNullOrEmpty(result))
        //        {
        //            var listaConsultada = JsonConvert.DeserializeObject<List<Anexo>>(result);

        //            if (listaConsultada?.Count > 0)
        //                listaDeAnexo.AddRange(listaConsultada);
        //        }

        //        foreach (var anexo in listaDeAnexo)
        //        {
        //            var uriDownload = new Uri(string.Format(Urls.DownloadDeAnexo, tramite, protocolo, anexo.Chave));
        //            var arquivo = _clientWeb.DownloadData(uriDownload);

        //            if (arquivo?.Length > 0)
        //                anexo.Conteudo = arquivo;
        //        }
        //    }

        //    return listaDeAnexo;
        //}

        //private List<Anexo> ObtenhaListaDeAnexos(HtmlNode coluna)
        //{
        //    var botaoAnexos = coluna?.SelectSingleNode("//button[@id='btnAnexo']");
        //    var lista = new List<Anexo>();

        //    if (botaoAnexos != null)
        //    {
        //        var onclick = botaoAnexos.Attributes["onclick"].Value;

        //        if (!string.IsNullOrEmpty(onclick))
        //        {
        //            var tramite = Regex.Match(onclick, @"javascript:abrirModalAnexo\(\s*\'(?<tramite>[\w]+)\'\s*\,\s*\'").Groups["tramite"].ToString().Trim();
        //            var protocolo = Regex.Match(onclick, @"\,\s*\'(?<protocolo>.+)\'\s*\,").Groups["protocolo"].ToString().Trim();
        //            var listaDeAnexos = DownloadListaDeAnexos(tramite, protocolo);

        //            if (listaDeAnexos?.Count > 0)
        //                lista.AddRange(listaDeAnexos);
        //        }
        //    }

        //    return lista;
        //}

        //private List<Detalhe> ObtenhaListaDeDetalhes(HtmlNode coluna)
        //{
        //    var botoesDetalhes = coluna?.SelectNodes("//button[@id='btnTramite']");
        //    var lista = new List<Detalhe>();

        //    if (botoesDetalhes != null)
        //    {
        //        foreach (var botao in botoesDetalhes)
        //        {
        //            var onclick = botao.Attributes["onclick"].Value;

        //            if (!string.IsNullOrEmpty(onclick))
        //            {
        //                var tramite = Regex.Match(onclick, @"javascript:mostrarTramite\(\s*(?<tramite>[\w]+)\s*\,\s*\'").Groups["tramite"].ToString().Trim();
        //                var protocolo = Regex.Match(onclick, @"\,\s*\'(?<protocolo>.+)\'\s*\,").Groups["protocolo"].ToString().Trim();
        //                var detalhe = ConsulteDetalhe(tramite, protocolo);

        //                if (detalhe != null)
        //                    lista.Add(detalhe);
        //            }
        //        }
        //    }

        //    return lista;
        //}

        private void ObtenhaValorCampoDoProtocolo(Protocolo protocolo, HtmlNode htmlNode)
        {
            string[] campoValor = htmlNode.InnerText.Split(':');

            if (campoValor?.Length >= 2)
            {
                var campo = campoValor[0]?.Trim();
                var valor = campoValor[1]?.Trim();

                if (!string.IsNullOrEmpty(campo))
                {
                    switch (campo)
                    {
                        case CamposProtocolo.GESTOR:
                            protocolo.Gestor = valor;
                            break;

                        case CamposProtocolo.DATA_ABERTURA:
                            protocolo.DataDeAbertura = valor;
                            break;

                        case CamposProtocolo.COMO_CONTRATOU:
                            protocolo.ComoContratou = valor;
                            break;

                        case CamposProtocolo.ASSUNTO:
                            protocolo.Assunto = valor;
                            break;

                        case CamposProtocolo.AREA:
                            protocolo.Area = valor;
                            break;

                        case CamposProtocolo.PROBLEMA:
                            protocolo.Problema = valor;
                            break;

                        case CamposProtocolo.PROTOCOLO:
                            protocolo.NumeroProtocolo = valor;
                            break;

                        case CamposProtocolo.SITUACAO:
                            protocolo.Situacao = valor;
                            break;

                        case CamposProtocolo.PRAZO_RESPOSTA:
                            protocolo.PrazoParaRespostaDoFornecedor = valor;
                            break;
                    }
                }
            }
        }

        private void ObtenhaValoresDadosProtocoloDiv(Protocolo protocolo, string classeDivPai)
        {
            var seletor = string.Format("//div[contains(@class, '{0}')]", classeDivPai);
            var divPai = _htmlDoc.DocumentNode.SelectSingleNode(seletor);
            var divfilhos = divPai?.SelectNodes("div");

            if (divfilhos != null)
            {
                foreach (var div in divfilhos)
                {
                    var itens = div?.SelectNodes("p");

                    if (itens?.Count > 0)
                    {
                        foreach (var p in itens)
                        {
                            ObtenhaValorCampoDoProtocolo(protocolo, p);
                        }
                    }
                    else
                    {
                        ObtenhaValorCampoDoProtocolo(protocolo, div);
                    }
                }
            }
        }

        private void PreenchaDadosPessoais(Protocolo protocolo)
        {
            protocolo.Nome = _htmlDoc.DocumentNode.SelectSingleNode("//span[contains(@class, 'visao-titulo')]")?.InnerText;

            var tableVisao = _htmlDoc.DocumentNode.SelectSingleNode("//table[contains(@class, 'tabela-visao')]");
            var linhasTableVisao = tableVisao?.SelectNodes("tr");

            if (linhasTableVisao != null)
            {
                foreach (var linha in linhasTableVisao)
                {
                    var campo = linha?.SelectNodes("th")?[0]?.InnerText.Trim();
                    var valor = linha?.SelectNodes("td")?[0]?.InnerText.Trim();

                    if (!string.IsNullOrEmpty(campo))
                    {
                        switch (campo)
                        {
                            case CamposProtocolo.CPF:
                                protocolo.Cpf = valor;
                                break;

                            case CamposProtocolo.CEP:
                                protocolo.Cep = valor;
                                break;

                            case CamposProtocolo.ENDERECO:
                                protocolo.Endereco = valor;
                                break;

                            case CamposProtocolo.TELEFONE:
                                protocolo.Telefone = valor;
                                break;

                            case CamposProtocolo.CELULAR:
                                protocolo.Celular = valor;
                                break;

                            case CamposProtocolo.EMAIL:
                                protocolo.Email = valor;
                                break;
                        }
                    }
                }
            }
        }

        private void PreenchaDadosProtocolo(Protocolo protocolo)
        {
            ObtenhaValoresDadosProtocoloDiv(protocolo, "table-detalhar");
            ObtenhaValoresDadosProtocoloDiv(protocolo, "status-visao");

            protocolo.ProtocolosFornecidos = _htmlDoc.DocumentNode.SelectSingleNode("//pre[contains(@class, 'texto-protocolo-empresa-pre')]")?.InnerText;
            protocolo.DescricaoDaReclamacao = _htmlDoc.DocumentNode.SelectSingleNode("//pre[contains(@class, 'texto-descricao-reclamacao-pre')]")?.InnerText;
            protocolo.PedidoAEmpresa = _htmlDoc.DocumentNode.SelectSingleNode("//pre[contains(@class, 'texto-pedido-reclamacao-pre')]")?.InnerText;
        }

        private void PreenchaInformacoesAdicionais(Protocolo protocolo)
        {
            var tableInfoAdicionais = _htmlDoc.DocumentNode.SelectSingleNode("//table[@id='atributosEspecificos']");
            var linhasTableInfoAdicionais = tableInfoAdicionais?.SelectSingleNode("tbody")?.SelectNodes("tr");
            var infoAdicionais = new StringBuilder();

            if (linhasTableInfoAdicionais != null)
            {
                foreach (var linha in linhasTableInfoAdicionais)
                {
                    var colunas = linha?.SelectNodes("td");

                    if (colunas?.Count >= 2)
                    {
                        infoAdicionais.AppendFormat("{0}: {1}", colunas[0]?.InnerText.Trim(), colunas[1]?.InnerText.Trim());
                        infoAdicionais.AppendLine();
                    }
                }
            }

            protocolo.InformacoesAdicionais = infoAdicionais.ToString();
        }

        //private void PreenchaListaDeAnotacoes(Protocolo protocolo)
        //{
        //    var linhasTableAndamento = _htmlDoc.DocumentNode.SelectNodes("//div[.='Andamento']/parent::div//table//tbody//tr");

        //    if (linhasTableAndamento != null)
        //    {
        //        foreach (var linha in linhasTableAndamento)
        //        {
        //            var colunas = linha?.SelectNodes("td");

        //            if (colunas != null && colunas.Count > 0)
        //            {
        //                var data = colunas[0]?.InnerText.Trim();

        //                var anotacao = new Anotacao()
        //                {
        //                    Data = string.IsNullOrEmpty(data) ? DateTime.Now.Date : DateTime.ParseExact(data, "dd/MM/yyyy", null),
        //                    Descricao = colunas[1]?.InnerText.Trim(),
        //                    Autor = colunas[2]?.InnerText.Trim()
        //                };

        //                anotacao.Detalhes.AddRange(ObtenhaListaDeDetalhes(colunas[3]));
        //                anotacao.Anexos.AddRange(ObtenhaListaDeAnexos(colunas[3]));

        //                protocolo.ListaDeAnotacao.Add(anotacao);
        //            }
        //        }
        //    }
        //}
    }
}