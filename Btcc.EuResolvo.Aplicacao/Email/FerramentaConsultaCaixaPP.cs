﻿using Btcc.Email.Exchange;
using Btcc.Infraestrutura.Util;
using Microsoft.Exchange.WebServices.Data;
using System;

namespace Btcc.EuResolvo.Aplicacao.Email
{
    public class FerramentaConsultaCaixaPP
    {
        public FindItemsResults<Item> ObtenhaEmails()
        {
            var service = new Service();
            var usuario = DadosDeConfiguracao.ObtenhaValor("EmailUsuario", true);
            var email = DadosDeConfiguracao.ObtenhaValor("Email", true);
            var senha = DadosDeConfiguracao.ObtenhaValor("EmailSenha", true);
            var caixaPP = DadosDeConfiguracao.ObtenhaValor("EmailCaixaPP", true);
            var dominio = DadosDeConfiguracao.ObtenhaValor("EmailDominio", true);
            var infoExchange = new DadosExchange(usuario, senha, dominio, email, caixaPP);
            return service.BuscarItemsPorPeriodo(infoExchange, DateTime.Now.AddDays(-1), DateTime.Now);
        }
    }
}
