﻿using Btcc.EuResolvo.Aplicacao.Entidades;
using CsvHelper.Configuration;

namespace Btcc.EuResolvo.Aplicacao.Csv
{
    public class EuResolvoMap : CsvClassMap<EuResolvoEnt>
    {
        public EuResolvoMap()
        {
            Map(x => x.IdEuResolvo).Index(0);
            Map(x => x.ProtocoloEuResolvo).Index(1);
            Map(x => x.ProtocoloSac).Index(2);
            Map(x => x.StatusEuResolvo).Index(5);
            Map(x => x.NomeCliente).Index(22);
            Map(x => x.CpfCnpjCliente).Index(23);
            Map(x => x.TipoCliente).Index(24);
            Map(x => x.TelefoneCliente).Index(26);
            Map(x => x.TelefoneReclamado).Index(27);
            //Map(x => x.MotivoSolicitacao).Index(28);
        }
    }
}