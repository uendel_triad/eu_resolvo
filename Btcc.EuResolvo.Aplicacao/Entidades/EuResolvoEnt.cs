﻿using Btcc.Infraestrutura.Extensoes;
using System;

namespace Btcc.EuResolvo.Aplicacao.Entidades
{
    public class EuResolvoEnt
    {
        private string _idEuResolvo;
        private string _protocoloEuResolvo;

        [Newtonsoft.Json.JsonProperty("Id Eu Resolvo")]
        public string IdEuResolvo
        {
            get
            {
                return _idEuResolvo;
            }
            set
            {
                _idEuResolvo = value.RemovaEspacosECaracteres();
            }
        }

        [Newtonsoft.Json.JsonProperty("Protocolo Eu Resolvo")]
        public string ProtocoloEuResolvo
        {
            get
            {
                return _protocoloEuResolvo;
            }
            set
            {
                _protocoloEuResolvo = value.RemovaEspacosECaracteres();
            }
        }

        [Newtonsoft.Json.JsonProperty("Nome Cliente")]
        public string NomeCliente { get; set; }

        [Newtonsoft.Json.JsonProperty("CPF/CNPJ")]
        public string CpfCnpjCliente { get; set; }

        [Newtonsoft.Json.JsonProperty("Tipo Cliente")]
        public string TipoCliente { get; set; }

        [Newtonsoft.Json.JsonProperty("Telefone Cliente")]
        public string TelefoneCliente { get; set; }

        [Newtonsoft.Json.JsonProperty("Telefone Reclamado")]
        public string TelefoneReclamado { get; set; }

        public string StatusEuResolvo { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ProtocoloSac { get; set; }

        //public string MotivoSolicitacao { get; set; }

        // Os parâmetros abaixo são requeridos pela API
        public string Mailing { get; set; }
        public long Id { get; set; }
        public string Status { get; set; }
        public string NomeDaFila { get; set; }
        public string SubStatus { get; set; }
        public DateTime? DataDeVencimento { get; set; }

        // Melhoria solicitada pelo Dilson, relacionada ao projeto do Unificado Ouvidoria (2020-09-03)
        public int? Responsavel { get; set; }
    }
}